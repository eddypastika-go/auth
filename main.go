package main

import (
	"runtime"

	"gitlab.com/eddypastika-go/auth/cmd"
	"gitlab.com/eddypastika-go/auth/src"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	cmd.Start(src.Setup())
}
