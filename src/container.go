package src

import (
	puputanGRPC "gitlab.com/eddypastika-go/auth/src/domain/repository/grpc/notify"
	endpointrepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/endpoint"
	endpointgrouprepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/endpointgroup"
	servicerepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/service"
	userrepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	uarepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/useraccess"
	verifrepo "gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/verification"
	endpointusecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/endpoint"
	endpointgroupusecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/endpointgroup"
	serviceusecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/service"
	userusecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/user"
	uausecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/useraccess"
	verifusecase "gitlab.com/eddypastika-go/auth/src/domain/usecase/verification"
	"gitlab.com/eddypastika-go/auth/src/shared/config"
	"gitlab.com/eddypastika-go/auth/src/shared/database"
	logs "gitlab.com/eddypastika-go/auth/src/shared/log"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/redis"
	tokenauth "gitlab.com/eddypastika-go/auth/src/shared/token"
	"gitlab.com/eddypastika-go/utilities/logger"
	"gitlab.com/eddypastika-go/utilities/secret"
	"gitlab.com/eddypastika-go/utilities/token"
)

// Container to store all constructors
type Container struct {
	Config   *config.Config
	Logger   logger.Logger
	Resource *model.Resource

	EndpointUsecase      endpointusecase.Usecase
	EndpointGroupUsecase endpointgroupusecase.Usecase
	ServiceUsecase       serviceusecase.Usecase
	UserUsecase          userusecase.Usecase
	UserAccessUsecase    uausecase.Usecase
	VerificationUsecase  verifusecase.Usecase
}

// Setup build container
func Setup() *Container {
	// - setup resource
	cfg := config.New()
	log := logs.New(cfg)
	db := database.New(cfg)
	cache := redis.New(cfg)
	tokenUtil := token.New(cfg.TokenSecret)
	tokenAuth := tokenauth.New(cfg)
	secretHelper := secret.New()

	resource := &model.Resource{
		Config:    cfg,
		DB:        db,
		Redis:     cache,
		Logger:    log,
		TokenUtil: tokenUtil,
		TokenAuth: tokenAuth,
		Secret:    secretHelper,
	}

	// - setup repo
	puputanGrpc := puputanGRPC.New(resource)
	userRepo := userrepo.New(resource)
	uaRepo := uarepo.New(resource)
	svcRepo := servicerepo.New(resource)
	endpointRepo := endpointrepo.New(resource)
	endpointGroupRepo := endpointgrouprepo.New(resource)
	verifRepo := verifrepo.New(resource)

	// - setup usecase
	svcUsecase := serviceusecase.New(svcRepo, resource)
	userUsecase := userusecase.New(userRepo, uaRepo, svcRepo, puputanGrpc, resource)
	uaUsecase := uausecase.New(userRepo, uaRepo, svcRepo, resource)
	endpointUsecase := endpointusecase.New(endpointRepo, resource)
	endpointGroupUsecase := endpointgroupusecase.New(endpointGroupRepo, endpointRepo, svcRepo, resource)
	verifUsecase := verifusecase.New(userRepo, verifRepo, puputanGrpc, resource)

	return &Container{
		Config:               cfg,
		Logger:               log,
		Resource:             resource,
		EndpointUsecase:      endpointUsecase,
		EndpointGroupUsecase: endpointGroupUsecase,
		ServiceUsecase:       svcUsecase,
		UserUsecase:          userUsecase,
		UserAccessUsecase:    uaUsecase,
		VerificationUsecase:  verifUsecase,
	}
}
