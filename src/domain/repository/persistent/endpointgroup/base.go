package endpointgroup

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Endpoint Group Repository"

type (
	Repository interface {
		Insert(ctx *isession.Isession, content dao.EndpointGroup) (err error)
		GetByID(ctx *isession.Isession, id uint64) (res dao.EndpointGroup, err error)
		Delete(ctx *isession.Isession, content dao.EndpointGroup) (err error)
		Update(ctx *isession.Isession, where, content dao.EndpointGroup) (err error)
		GetByServiceAndEndpoint(ctx *isession.Isession, svcID, endpointID uint64) (res dao.EndpointGroup, err error)

		SetUnscoped(active bool)
	}

	repository struct {
		resource   *model.Resource
		isUnscoped bool
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}

func (r *repository) SetUnscoped(active bool) {
	r.isUnscoped = active
}
