package endpointgroup

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) Update(ctx *isession.Isession, where, content dao.EndpointGroup) (err error) {
	db := r.resource.GetDB()
	if r.isUnscoped {
		db = db.Unscoped()
	}

	err = db.WithContext(ctx.Ctx).Where(where).Updates(&content).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorUpdate, err)
		return
	}

	return
}
