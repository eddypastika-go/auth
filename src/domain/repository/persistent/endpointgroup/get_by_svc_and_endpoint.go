package endpointgroup

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetByServiceAndEndpoint(
	ctx *isession.Isession, svcID, endpointID uint64) (res dao.EndpointGroup, err error) {
	db := r.resource.GetDB()
	if r.isUnscoped {
		db = db.Unscoped().Where("deleted_at is not null")
	}

	err = db.WithContext(ctx.Ctx).
		Where("service_id = ?", svcID).
		Where("endpoint_id = ?", endpointID).
		First(&res).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	return
}
