package user

import (
	"fmt"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetCacheByEmailAndType(
	ctx *isession.Isession, email, userType string) (res dao.User, err error) {
	res, err = r.getCacheUserLogin(ctx, email, userType)
	if err != nil {
		ctx.ClearError()

		err = r.resource.GetDB().WithContext(ctx.Ctx).
			Where("email = ?", email).Where("type = ?", userType).First(&res).Error
		if err != nil {
			ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
			return
		}

		go r.SetCacheByEmailAndType(ctx, res)
	}

	return
}

func keyEmailAndType(email, userType string) string {
	return fmt.Sprintf("#user#%s#%s", userType, email)
}

func (r *repository) getCacheUserLogin(ctx *isession.Isession, email, userType string) (res dao.User, err error) {
	err = r.resource.Redis.Get(ctx, keyEmailAndType(email, userType), &res)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (r *repository) SetCacheByEmailAndType(ctx *isession.Isession, data dao.User) error {
	err := r.resource.Redis.Set(
		ctx, keyEmailAndType(data.Email, data.Type), data, r.resource.Config.RedisDefaultTTL,
	)

	if err != nil {
		return err
	}

	return nil
}

func (r *repository) DeleteCacheByEmailAndType(ctx *isession.Isession, email, userType string) (err error) {
	return r.resource.Redis.Delete(ctx, keyEmailAndType(email, userType))
}
