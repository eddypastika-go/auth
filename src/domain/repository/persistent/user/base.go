package user

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "User Repository"

type (
	Repository interface {
		Insert(ctx *isession.Isession, content dao.User) (res dao.User, err error)
		Update(ctx *isession.Isession, where, content dao.User) (err error)
		Delete(ctx *isession.Isession, content dao.User) (err error)
		GetByID(ctx *isession.Isession, id uint64) (res dao.User, err error)
		GetOne(ctx *isession.Isession, where dao.User) (res dao.User, err error)

		GetCacheByEmailAndType(ctx *isession.Isession, email, userType string) (res dao.User, err error)
		SetCacheByEmailAndType(ctx *isession.Isession, data dao.User) (err error)
		DeleteCacheByEmailAndType(ctx *isession.Isession, email, userType string) (err error)
	}

	repository struct {
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}
