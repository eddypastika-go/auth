package verification

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) Delete(ctx *isession.Isession, content dao.Verification) (err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Delete(&content, content).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorDelete, err)
		return
	}

	return
}
