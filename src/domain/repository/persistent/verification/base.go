package verification

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Verification Repository"

type (
	Repository interface {
		Insert(ctx *isession.Isession, content dao.Verification) (res dao.Verification, err error)
		Update(ctx *isession.Isession, where, content dao.Verification) (err error)
		Delete(ctx *isession.Isession, content dao.Verification) (err error)
		DeleteBeforeNow(ctx *isession.Isession, content dao.Verification, now time.Time) (err error)
		GetOne(ctx *isession.Isession, where dao.Verification) (res dao.Verification, err error)
		GetLastOne(ctx *isession.Isession, where dao.Verification) (res dao.Verification, err error)
		GetLastOneByEmail(ctx *isession.Isession, email string) (res dao.Verification, err error)
	}

	repository struct {
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}
