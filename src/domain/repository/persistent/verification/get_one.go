package verification

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetOne(
	ctx *isession.Isession, where dao.Verification) (res dao.Verification, err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Where(where).First(&res).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	return
}
