package verification

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetLastOneByEmail(
	ctx *isession.Isession, email string) (res dao.Verification, err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Where("email = ?", email).Last(&res).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	return
}
