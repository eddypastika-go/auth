package verification

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) DeleteBeforeNow(
	ctx *isession.Isession, content dao.Verification, now time.Time) (err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Where("created_at < ?", now).Delete(&content, content).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorDelete, err)
		return
	}

	return
}
