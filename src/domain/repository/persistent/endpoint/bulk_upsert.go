package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
	"gorm.io/gorm/clause"
)

func (r *repository) BulkUpsert(ctx *isession.Isession, items []dao.Endpoint) (err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "path"}, {Name: "method"}},
		DoNothing: true,
	}).CreateInBatches(items, 500).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorUpsert, err)
		return
	}

	return
}
