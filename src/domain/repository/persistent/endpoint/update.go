package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) Update(ctx *isession.Isession, where, content dao.Endpoint) (err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Where(where).Updates(&content).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorUpdate, err)
		return
	}

	return
}
