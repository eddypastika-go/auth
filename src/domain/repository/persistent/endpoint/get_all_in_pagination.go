package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/queris"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetAllInPagination(
	ctx *isession.Isession, page *queris.Paging,
) (res queris.PaginationRes, err error) {
	var (
		count int64
		items []dao.Endpoint
	)

	sortList := map[string]string{
		"createdAt":    "created_at",
		"id":           "id",
		"path":         "path",
		"method":       "method",
		"service_name": "service_name",
	}

	err = r.resource.GetDB().Model(&dao.Endpoint{}).Count(&count).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	err = r.resource.GetDB().Scopes(page.Paginate(sortList, count, &res)).Find(&items).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	res.Items = items

	return
}
