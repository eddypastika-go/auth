package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/queris"
)

var msgLog = "Endpoint Repository"

type (
	Repository interface {
		BulkUpsert(ctx *isession.Isession, items []dao.Endpoint) (err error)
		Delete(ctx *isession.Isession, content dao.Endpoint) (err error)
		GetByID(ctx *isession.Isession, id uint64) (res dao.Endpoint, err error)
		GetAllInPagination(ctx *isession.Isession, page *queris.Paging) (res queris.PaginationRes, err error)
		Update(ctx *isession.Isession, where, content dao.Endpoint) (err error)
	}

	repository struct {
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}
