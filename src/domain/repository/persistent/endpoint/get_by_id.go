package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (r *repository) GetByID(ctx *isession.Isession, id uint64) (res dao.Endpoint, err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Where("id = ?", id).First(&res).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorGet, err)
		return
	}

	return
}
