package service

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Service Repository"

type (
	Repository interface {
		Insert(ctx *isession.Isession, content dao.Service) (err error)
		GetByID(ctx *isession.Isession, id uint64) (res dao.Service, err error)
		GetOne(ctx *isession.Isession, where dao.Service) (res dao.Service, err error)
	}

	repository struct {
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}
