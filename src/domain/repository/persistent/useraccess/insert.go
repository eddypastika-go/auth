package useraccess

import (
	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (r *repository) Insert(ctx *isession.Isession, content dao.UserAccess) (err error) {
	err = r.resource.GetDB().WithContext(ctx.Ctx).Save(&content).Error
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeDBErrorInsert, err)
		return
	}

	return
}
