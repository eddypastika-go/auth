package useraccess

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "User Access Repository"

type (
	Repository interface {
		Insert(ctx *isession.Isession, content dao.UserAccess) (err error)
	}

	repository struct {
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	return &repository{resource: resource}
}
