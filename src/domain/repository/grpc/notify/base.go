package notify

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/grpcis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Notify Repository"

type (
	Repository interface {
		SendMail(ctx *isession.Isession, req *grpc.SendMailRequest) (res grpc.SendMailResponse, err error)
	}

	repository struct {
		grpcCon  *grpcis.RpcConnection
		client   grpc.NotificationHandlerClient
		resource *model.Resource
	}
)

func New(resource *model.Resource) Repository {
	cfg := resource.Config
	grpcCon := grpcis.NewGRPCConnection(cfg.ToGRPCOpt(cfg.NotifyGrpcAddress))

	return &repository{
		grpcCon:  grpcCon,
		client:   grpc.NewNotificationHandlerClient(grpcCon.Conn),
		resource: resource,
	}
}
