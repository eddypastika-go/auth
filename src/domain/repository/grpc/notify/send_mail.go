package notify

import (
	"context"

	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"

	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/utilities/isession"
)

func (r *repository) SendMail(
	is *isession.Isession, req *grpc.SendMailRequest) (res grpc.SendMailResponse, err error) {
	ctx := r.grpcCon.CreateContext(context.Background(), is)

	out, err := r.client.SendMail(ctx, req)
	if err != nil {
		is.Error(isession.GetSource(), msgLog, varis.CodeGRPCError, err)
		return
	}

	if out == nil {
		err = varis.ErrorGRPCEmptyResult
		is.Error(isession.GetSource(), msgLog, varis.CodeGRPCEmptyResult, err)
		return
	}

	res.Message = out.Message
	res.Status = out.Status

	return
}
