package endpointgroup

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gorm.io/gorm"
)

func (u *usecase) DeleteByID(ctx *isession.Isession, id uint64, ul entitis.TokenPayload) (err error) {
	eg, err := u.endpointGroupRepository.GetByID(ctx, id)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorEndpointGroupNotFound.Error()
		return
	}

	err = u.endpointGroupRepository.Update(ctx, eg, dao.EndpointGroup{
		UpdatedBy: ul.Email,
		Base: dao.Base{
			DeletedAt: gorm.DeletedAt{Time: time.Now(), Valid: true},
		},
	})
	if err != nil {
		return
	}

	return
}
