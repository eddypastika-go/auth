package endpointgroup

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	endpointgroupdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpointgroup"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gorm.io/gorm"
)

func (u *usecase) Create(ctx *isession.Isession, req endpointgroupdto.CreateReq, ul entitis.TokenPayload) (err error) {
	egCheck := u.checkExist(ctx, &req)
	if egCheck != nil {
		err = u.recreate(ctx, ul.Email, egCheck)
		if err != nil {
			return
		}
		return
	}

	err = u.createNew(ctx, &req, &ul)
	if err != nil {
		return
	}

	return
}

func (u *usecase) setEndpointGroupFromCreateReq(
	req *endpointgroupdto.CreateReq, ul *entitis.TokenPayload) (endpointGroup dao.EndpointGroup) {
	endpointGroup = dao.EndpointGroup{
		ServiceID:  req.ServiceID,
		EndpointID: req.EndpointID,
		Role:       req.Role,
		UpdatedBy:  ul.Email,
	}

	return
}

func (u *usecase) checkExist(ctx *isession.Isession, req *endpointgroupdto.CreateReq) *dao.EndpointGroup {
	u.endpointGroupRepository.SetUnscoped(true)
	eg, err := u.endpointGroupRepository.GetByServiceAndEndpoint(ctx, req.ServiceID, req.EndpointID)
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil
	}

	u.endpointGroupRepository.SetUnscoped(false)
	return &eg
}

func (u *usecase) recreate(ctx *isession.Isession, email string, egCheck *dao.EndpointGroup) (err error) {
	now := time.Now()
	err = u.endpointGroupRepository.Update(ctx, *egCheck, dao.EndpointGroup{
		UpdatedBy: email,
		Base: dao.Base{
			DeletedAt: gorm.DeletedAt{Time: now, Valid: false},
			UpdatedAt: &now,
		},
	})

	return
}

func (u *usecase) createNew(ctx *isession.Isession, req *endpointgroupdto.CreateReq, ul *entitis.TokenPayload) (err error) {
	_, err = u.svcRepository.GetByID(ctx, req.ServiceID)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorServiceNotFound.Error()
		return
	}

	_, err = u.endpointRepository.GetByID(ctx, req.EndpointID)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorEndpointNotFound.Error()
		return
	}

	err = u.endpointGroupRepository.Insert(ctx, u.setEndpointGroupFromCreateReq(req, ul))
	if err != nil {
		return
	}

	return
}
