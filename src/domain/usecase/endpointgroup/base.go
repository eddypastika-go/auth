package endpointgroup

import (
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/endpoint"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/endpointgroup"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/service"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	endpointgroupdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpointgroup"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Endpoint Group Usecase"

type (
	Usecase interface {
		Create(ctx *isession.Isession, req endpointgroupdto.CreateReq, ul entitis.TokenPayload) (err error)
		DeleteByID(ctx *isession.Isession, reqID uint64, ul entitis.TokenPayload) (err error)
	}

	usecase struct {
		endpointGroupRepository endpointgroup.Repository
		endpointRepository      endpoint.Repository
		svcRepository           service.Repository
		resource                *model.Resource
	}
)

func New(
	endpointGroupRepository endpointgroup.Repository,
	endpointRepository endpoint.Repository,
	svcRepository service.Repository,
	resource *model.Resource,
) Usecase {
	return &usecase{
		endpointGroupRepository: endpointGroupRepository,
		endpointRepository:      endpointRepository,
		svcRepository:           svcRepository,
		resource:                resource,
	}
}
