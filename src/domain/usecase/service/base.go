package service

import (
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/service"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	servicedto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/service"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Service Usecase"

type (
	Usecase interface {
		Create(ctx *isession.Isession, req servicedto.CreateReq, userLogin entitis.TokenPayload) (err error)
	}
	usecase struct {
		svcRepository service.Repository
		resource      *model.Resource
	}
)

func New(svcRepository service.Repository, resource *model.Resource) Usecase {
	return &usecase{
		svcRepository: svcRepository,
		resource:      resource,
	}
}
