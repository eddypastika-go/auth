package service

import (
	"strings"

	"gitlab.com/eddypastika-go/utilities/entitis"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	servicedto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/service"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (u *usecase) Create(ctx *isession.Isession, req servicedto.CreateReq, ul entitis.TokenPayload) (err error) {
	err = u.svcRepository.Insert(ctx, u.setServiceFromCreateReq(&req, &ul))
	if err != nil {
		return
	}

	return
}

func (u *usecase) setServiceFromCreateReq(req *servicedto.CreateReq, ul *entitis.TokenPayload) (svc dao.Service) {
	code, secret := u.resource.Secret.GenerateServiceCredential(req.Name)
	svc = dao.Service{
		Name:       strings.ToLower(strings.ReplaceAll(req.Name, " ", "-")),
		Code:       code,
		Secret:     secret,
		UpdatedBy:  ul.Email,
		IsExternal: req.IsExternal,
		IsActive:   true,
	}

	return
}
