package verification

import (
	"strings"

	"gitlab.com/eddypastika-go/auth/src/domain/usecase/verification/confirm_adapter"
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) Confirm(ctx *isession.Isession, req verifdto.ConfirmReq) (err error) {
	adapter, err := u.setConfirmAdapter(req.Template)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	err = adapter.ConfirmVerificationCode(ctx, req)
	if err != nil {
		return
	}

	return
}

func (u *usecase) setConfirmAdapter(template string) (res confirm_adapter.IConfirm, err error) {
	template = strings.ToLower(template)

	listTemplate := map[string]confirm_adapter.IConfirm{
		constant.TypeBrandSignupVerification:       confirm_adapter.NewSignup(u.setConfirmAdapterParam(), false),
		constant.TypeClientSignupVerification:      confirm_adapter.NewSignup(u.setConfirmAdapterParam(), false),
		constant.TypeChangePasswordVerification:    confirm_adapter.NewDefault(u.setConfirmAdapterParam(), false),
		constant.TypeForgotPasswordVerification:    confirm_adapter.NewDefault(u.setConfirmAdapterParam(), false),
		constant.TypeChangeEmailVerification:       confirm_adapter.NewDefault(u.setConfirmAdapterParam(), false),
		constant.TypeDeactivateAccountVerification: confirm_adapter.NewDefault(u.setConfirmAdapterParam(), false),
	}

	res, ok := listTemplate[template]
	if !ok {
		err = constant.ErrorInvalidVerificationTemplate
		return
	}

	return
}

func (u *usecase) setConfirmAdapterParam() *confirm_adapter.ConfirmAdapter {
	return &confirm_adapter.ConfirmAdapter{
		UserRepository:   u.userRepository,
		VerifyRepository: u.verifyRepository,
		PuputanGRPC:      u.puputanGRPC,
		Resource:         u.resource,
	}
}
