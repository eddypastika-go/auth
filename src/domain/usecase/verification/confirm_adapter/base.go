package confirm_adapter

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/domain/repository/grpc/notify"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/verification"
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

const VerifyType = "success"

type IConfirm interface {
	ConfirmVerificationCode(ctx *isession.Isession, req verifdto.ConfirmReq) error
}

type ConfirmAdapter struct {
	UserRepository   user.Repository
	VerifyRepository verification.Repository
	PuputanGRPC      notify.Repository
	Resource         *model.Resource
}

func (c *ConfirmAdapter) VerifyRequest(
	ctx *isession.Isession, msgLog string, req verifdto.ConfirmReq) (verify dao.Verification, err error) {
	verify, err = c.VerifyRepository.GetLastOne(ctx, dao.Verification{Email: req.Email, Template: req.Template})
	if err != nil {
		err = constant.ErrorVerificationNotFound
		ctx.Error(isession.GetSource(), msgLog, varis.CodeNotFound, err)
		return
	}

	if verify.Code != req.Code {
		err = constant.ErrorInvalidVerificationCode
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	if verify.ExpiredAt.Before(time.Now()) {
		err = constant.ErrorExpiredVerificationCode
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	return
}
