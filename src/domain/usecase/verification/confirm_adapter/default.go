package confirm_adapter

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gorm.io/gorm"
)

type Default struct {
	adapter                 *ConfirmAdapter
	sendSuccessNotification bool
}

func NewDefault(adapter *ConfirmAdapter, sendSuccessNotification bool) *Default {
	return &Default{adapter: adapter, sendSuccessNotification: sendSuccessNotification}
}

func (d *Default) ConfirmVerificationCode(ctx *isession.Isession, req verifdto.ConfirmReq) (err error) {
	msgLog := "Signup Confirm Verification Code Usecase"

	verify, err := d.adapter.VerifyRequest(ctx, msgLog, req)
	if err != nil {
		return
	}

	go d.adapter.VerifyRepository.Update(ctx, verify, dao.Verification{
		Base: dao.Base{
			DeletedAt: gorm.DeletedAt{Time: time.Now(), Valid: true},
		},
		IsVerified: true,
	})

	go func() {
		if d.sendSuccessNotification {
			d.adapter.PuputanGRPC.SendMail(
				ctx, verify.ToSendMailVerificationCode(ctx.XID, verify.ToDefaultSendParam(), VerifyType),
			)
		}
	}()

	return
}
