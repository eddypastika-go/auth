package confirm_adapter

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gorm.io/gorm"
)

type Signup struct {
	adapter                 *ConfirmAdapter
	sendSuccessNotification bool
}

func NewSignup(adapter *ConfirmAdapter, sendSuccessNotification bool) *Signup {
	return &Signup{adapter: adapter, sendSuccessNotification: sendSuccessNotification}
}

func (d *Signup) ConfirmVerificationCode(ctx *isession.Isession, req verifdto.ConfirmReq) (err error) {
	msgLog := "Signup Confirm Verification Code Usecase"

	verify, err := d.adapter.VerifyRequest(ctx, msgLog, req)
	if err != nil {
		return
	}

	err = d.setUserVerified(ctx, req)
	if err != nil {
		return
	}

	go d.adapter.VerifyRepository.Update(ctx, verify, dao.Verification{
		Base: dao.Base{
			DeletedAt: gorm.DeletedAt{Time: time.Now(), Valid: true},
		},
		IsVerified: true,
	})

	go func() {
		if d.sendSuccessNotification {
			d.adapter.PuputanGRPC.SendMail(
				ctx, verify.ToSendMailVerificationCode(ctx.XID, verify.ToDefaultSendParam(), VerifyType),
			)
		}
	}()

	return
}

func (d *Signup) setUserVerified(ctx *isession.Isession, req verifdto.ConfirmReq) (err error) {
	where := dao.User{
		Email:      req.Email,
		Type:       req.UserType,
		IsVerified: false,
		IsActive:   false,
	}

	err = d.adapter.UserRepository.Update(ctx, where, dao.User{IsVerified: true})

	return
}
