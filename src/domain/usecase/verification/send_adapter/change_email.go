package send_adapter

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
	"gorm.io/gorm"
)

type ChangeEmail struct {
	adapter *SendAdapter
}

func NewChangeEmail(adapter *SendAdapter) *ChangeEmail {
	return &ChangeEmail{adapter: adapter}
}

func (d *ChangeEmail) SendVerificationCode(ctx *isession.Isession, req verifdto.SendReq) (err error) {
	msgLog := "Change Email Send Verification Code Usecase"
	now := time.Now()

	foundUser, err := d.adapter.UserRepository.GetOne(ctx, dao.User{Email: req.Email, Type: req.UserType})
	if err == nil && foundUser.Email != varis.Empty {
		err = constant.ErrorEmailAlreadyExist
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	if err != gorm.ErrRecordNotFound {
		return
	}

	ctx.ClearError()

	verify, err := d.adapter.VerifyRepository.Insert(ctx, d.adapter.SetVerificationFromSendReq(req, now))
	if err != nil {
		return
	}

	go d.adapter.PuputanGRPC.SendMail(
		ctx, verify.ToSendMailVerificationCode(ctx.XID, verify.ToChangeEmailSendParam(), VerifyType),
	)

	return
}
