package send_adapter

import (
	"strings"
	"time"

	"gitlab.com/eddypastika-go/auth/src/domain/repository/grpc/notify"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/verification"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

const VerifyType = "request"

type ISend interface {
	SendVerificationCode(ctx *isession.Isession, req verifdto.SendReq) error
}

type SendAdapter struct {
	UserRepository   user.Repository
	VerifyRepository verification.Repository
	PuputanGRPC      notify.Repository
	Resource         *model.Resource
}

func (u *SendAdapter) DeleteExisting(ctx *isession.Isession, req verifdto.SendReq, now time.Time) (err error) {
	r := dao.Verification{
		Email:    req.Email,
		UserType: req.UserType,
		Template: strings.ToLower(req.Template),
	}

	err = u.VerifyRepository.DeleteBeforeNow(ctx, r, now)
	if err != nil {
		return
	}

	return
}

func (u *SendAdapter) SetVerificationFromSendReq(req verifdto.SendReq, now time.Time) (res dao.Verification) {
	code, err := u.Resource.Secret.GenerateVerificationCode(u.Resource.Config.NumberOTP)
	if err != nil {
		code = varis.Empty
	}

	res = dao.Verification{
		Email:      req.Email,
		UserType:   req.UserType,
		Code:       code,
		Template:   strings.ToLower(req.Template),
		ExpiredAt:  now.Add(u.Resource.Config.ExpiredOTP),
		IsVerified: false,
		Base:       dao.Base{CreatedAt: &now, UpdatedAt: &now},
	}

	return
}
