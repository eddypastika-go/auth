package send_adapter

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

type Default struct {
	adapter *SendAdapter
}

func NewDefault(adapter *SendAdapter) *Default {
	return &Default{adapter: adapter}
}

func (d *Default) SendVerificationCode(ctx *isession.Isession, req verifdto.SendReq) (err error) {
	msgLog := "Default Send Verification Code Usecase"
	now := time.Now()

	_, err = d.adapter.UserRepository.GetCacheByEmailAndType(ctx, req.Email, req.UserType)
	if err != nil {
		err = constant.ErrorUserNotFound
		ctx.Error(isession.GetSource(), msgLog, varis.CodeNotFound, err)
		return
	}

	go d.adapter.DeleteExisting(ctx, req, now)

	verify, err := d.adapter.VerifyRepository.Insert(ctx, d.adapter.SetVerificationFromSendReq(req, now))
	if err != nil {
		return
	}

	go d.adapter.PuputanGRPC.SendMail(
		ctx, verify.ToSendMailVerificationCode(ctx.XID, verify.ToDefaultSendParam(), VerifyType),
	)

	return
}
