package verification

import (
	"gitlab.com/eddypastika-go/auth/src/domain/repository/grpc/notify"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/verification"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Verification Usecase"

type (
	Usecase interface {
		Send(ctx *isession.Isession, req verifdto.SendReq) (err error)
		Confirm(ctx *isession.Isession, req verifdto.ConfirmReq) (err error)
	}
	usecase struct {
		userRepository   user.Repository
		verifyRepository verification.Repository
		puputanGRPC      notify.Repository
		resource         *model.Resource
	}
)

func New(
	userRepository user.Repository,
	verifRepository verification.Repository,
	puputanGRPC notify.Repository,
	resource *model.Resource,
) Usecase {
	return &usecase{
		userRepository:   userRepository,
		verifyRepository: verifRepository,
		puputanGRPC:      puputanGRPC,
		resource:         resource,
	}
}
