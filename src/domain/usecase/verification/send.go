package verification

import (
	"strings"

	"gitlab.com/eddypastika-go/auth/src/domain/usecase/verification/send_adapter"
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) Send(ctx *isession.Isession, req verifdto.SendReq) (err error) {
	adapter, err := u.setSendAdapter(req.Template)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	err = adapter.SendVerificationCode(ctx, req)
	if err != nil {
		return
	}

	return
}

func (u *usecase) setSendAdapter(template string) (res send_adapter.ISend, err error) {
	template = strings.ToLower(template)

	listTemplate := map[string]send_adapter.ISend{
		constant.TypeBrandSignupVerification:       send_adapter.NewDefault(u.setSendAdapterParam()),
		constant.TypeClientSignupVerification:      send_adapter.NewDefault(u.setSendAdapterParam()),
		constant.TypeChangePasswordVerification:    send_adapter.NewDefault(u.setSendAdapterParam()),
		constant.TypeForgotPasswordVerification:    send_adapter.NewDefault(u.setSendAdapterParam()),
		constant.TypeChangeEmailVerification:       send_adapter.NewChangeEmail(u.setSendAdapterParam()),
		constant.TypeDeactivateAccountVerification: send_adapter.NewDefault(u.setSendAdapterParam()),
	}

	res, ok := listTemplate[template]
	if !ok {
		err = constant.ErrorInvalidVerificationTemplate
		return
	}

	return
}

func (u *usecase) setSendAdapterParam() *send_adapter.SendAdapter {
	return &send_adapter.SendAdapter{
		UserRepository:   u.userRepository,
		VerifyRepository: u.verifyRepository,
		PuputanGRPC:      u.puputanGRPC,
		Resource:         u.resource,
	}
}
