package endpoint

import (
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/queris"
)

func (u *usecase) GetAllInPagination(ctx *isession.Isession, page *queris.Paging) (res queris.PaginationRes, err error) {
	res, err = u.endpointRepository.GetAllInPagination(ctx, page)
	if err != nil {
		return
	}

	return
}
