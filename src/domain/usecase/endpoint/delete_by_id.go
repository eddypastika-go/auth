package endpoint

import (
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
	"gorm.io/gorm"
)

func (u *usecase) DeleteByID(ctx *isession.Isession, id uint64, ul entitis.TokenPayload) (err error) {
	endpoint, err := u.endpointRepository.GetByID(ctx, id)
	if err != nil {
		err = constant.ErrorEndpointNotFound
		ctx.Error(isession.GetSource(), msgLog, varis.CodeNotFound, err)
		return
	}

	err = u.endpointRepository.Update(ctx, endpoint, dao.Endpoint{
		UpdatedBy: ul.Email,
		Base:      dao.Base{DeletedAt: gorm.DeletedAt{Time: time.Now(), Valid: true}},
	})
	if err != nil {
		return
	}

	return
}
