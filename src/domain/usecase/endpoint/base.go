package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/endpoint"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	endpointdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpoint"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/queris"
)

var msgLog = "Endpoint Usecase"

type (
	Usecase interface {
		BulkUpsert(ctx *isession.Isession, req []endpointdto.UpsertReq) (err error)
		DeleteByID(ctx *isession.Isession, id uint64, ul entitis.TokenPayload) (err error)
		GetAllInPagination(ctx *isession.Isession, page *queris.Paging) (res queris.PaginationRes, err error)
	}
	usecase struct {
		endpointRepository endpoint.Repository
		resource           *model.Resource
	}
)

func New(endpointRepository endpoint.Repository, resource *model.Resource) Usecase {
	return &usecase{
		endpointRepository: endpointRepository,
		resource:           resource,
	}
}
