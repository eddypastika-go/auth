package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	endpointdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpoint"

	"gitlab.com/eddypastika-go/utilities/isession"
)

func (u *usecase) BulkUpsert(ctx *isession.Isession, req []endpointdto.UpsertReq) (err error) {
	err = u.endpointRepository.BulkUpsert(ctx, setListEndpoints(req))
	if err != nil {
		return
	}

	return
}

func setListEndpoints(req []endpointdto.UpsertReq) []dao.Endpoint {
	res := make([]dao.Endpoint, len(req))

	for i, v := range req {
		res[i] = dao.Endpoint{
			Path:        v.Path,
			Method:      v.Method,
			Type:        v.Type,
			ServiceName: v.ServiceName,
			IsActive:    true,
		}
	}

	return res
}
