package user

import (
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	userdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) Signin(
	ctx *isession.Isession, req userdto.SigninReq, code, secret string) (res userdto.AuthRes, err error) {
	switch req.AuthType {
	case constant.Email:
		res, err = u.emailSignin(ctx, req.ToEmailSignin())
		if err != nil {
			return
		}
	case constant.Google:
		res, err = u.googleSignin(ctx, req.ToGoogleSignin(), code, secret)
		if err != nil {
			return
		}
	default:
		err = constant.ErrorInvalidAuthType
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBadRequest, err)
		return
	}

	return
}

func (u *usecase) emailSignin(ctx *isession.Isession, req userdto.EmailSignin) (res userdto.AuthRes, err error) {
	var foundUser dao.User

	if err = ctx.Echo.Validate(&req); err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeInvalidRequest, err)
		return
	}

	if foundUser, err = u.validateSignin(ctx, req.Email, req.UserType); err != nil {
		return
	}

	err = u.resource.Secret.CheckPassword(req.Password, foundUser.Password)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorWrong, constant.ErrorWrongPassword)
		return
	}

	token, err := u.resource.TokenAuth.GenerateTokenPair(foundUser.ToTokenPayload(req.UserType))
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenErrorGenerate, err)
		return
	}

	res = foundUser.ToAuthRes(token.AccessToken, token.RefreshToken)

	return
}

func (u *usecase) googleSignin(ctx *isession.Isession, req userdto.GoogleSignin, code, secret string) (res userdto.AuthRes, err error) {
	var foundUser dao.User

	if err = ctx.Echo.Validate(&req); err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeInvalidRequest, err)
		return
	}

	googleData, err := u.resource.TokenAuth.ValidateGoogleJWT(ctx, req.IDToken)
	if err != nil {
		return
	}

	if foundUser, err = u.validateSignin(ctx, googleData.Email, req.UserType); err != nil {
		if err == constant.ErrorUserNotFound {
			ctx.ClearError()
			res, err = u.Signup(
				ctx,
				userdto.SignupReq{
					Email:    googleData.Email,
					Password: varis.Empty,
					UserType: req.UserType,
					AuthType: constant.Google,
				},
				code,
				secret,
			)
			return
		}
		return
	}

	token, err := u.resource.TokenAuth.GenerateTokenPair(foundUser.ToTokenPayload(req.UserType))
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenErrorGenerate, err)
		return
	}

	res = foundUser.ToAuthRes(token.AccessToken, token.RefreshToken)

	return
}

func (u *usecase) validateSignin(ctx *isession.Isession, email, userType string) (foundUser dao.User, err error) {
	foundUser, err = u.userRepository.GetCacheByEmailAndType(ctx, email, userType)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorUserNotFound.Error()
		return
	}

	if foundUser.Password == varis.Empty {
		err = constant.ErrorUserEmptyPassword
		ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorEmpty, err)
		return
	}

	if !foundUser.IsActive {
		err = constant.ErrorUserInactive
		ctx.Error(isession.GetSource(), msgLog, varis.CodeUnauthorized, err)
		return
	}

	isForbidden := u.isForbiddenDomain(userType, email)
	if isForbidden {
		err = constant.ErrorForbiddenDomain
		ctx.Error(isession.GetSource(), msgLog, varis.CodeUnauthorized, err)
		return
	}

	return
}
