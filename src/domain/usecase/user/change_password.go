package user

import (
	"strings"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	userdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) ChangePassword(ctx *isession.Isession, req userdto.ChangePasswordReq, ul entitis.TokenPayload) (err error) {
	foundUser, err := u.userRepository.GetCacheByEmailAndType(ctx, ul.Email, ul.Type)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorUserNotFound.Error()
		return
	}

	err = u.resource.Secret.CheckPassword(req.OldPassword, foundUser.Password)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorWrong, constant.ErrorWrongPassword)
		return
	}

	newPwd, err := u.resource.Secret.GeneratePassword(req.NewPassword)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorGenerate, err)
		return
	}

	err = u.userRepository.Update(ctx, foundUser, dao.User{Password: newPwd})
	if err != nil {
		return
	}

	go u.userRepository.DeleteCacheByEmailAndType(ctx, ul.Email, ul.Type)
	go u.sendMailChangePasswordSuccess(ctx, ul.Email)

	return
}

func (u *usecase) sendMailChangePasswordSuccess(ctx *isession.Isession, email string) {
	emails := strings.Split(email, "@")
	bodyMail := []*grpc.KeyValue{
		{Key: "name", Value: emails[0]},
	}

	mailReq := &grpc.SendMailRequest{
		RequestID: ctx.XID,
		Content: &grpc.Mail{
			To:   email,
			Type: constant.TypeChangePasswordVerification + "_success",
			Body: bodyMail,
		},
	}

	u.puputanGRPC.SendMail(ctx, mailReq)

}
