package user

import (
	"strings"

	"gorm.io/gorm"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	userdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) Signup(ctx *isession.Isession, req userdto.SignupReq, code, secret string) (res userdto.AuthRes, err error) {
	svc, err := u.svcRepository.GetOne(ctx, dao.Service{Code: code, Secret: secret})
	if err != nil {
		ctx.ErrorMessage = constant.ErrorServiceNotFound.Error()
		return
	}

	userReq, err := u.setUserFromSignupReq(ctx, req)
	if err != nil {
		return
	}

	err = u.resource.DB.Transaction(func(tx *gorm.DB) error {
		u.resource.SetDBTrx(tx)
		defer u.resource.ClearDBTrx()

		user, errTx := u.userRepository.Insert(ctx, userReq)
		if errTx != nil {
			return errTx
		}

		errTx = u.uaRepository.Insert(ctx, u.setUserAccessFromSignupReq(user.ID, svc.ID))
		if errTx != nil {
			return errTx
		}

		token, errTx := u.resource.TokenAuth.GenerateTokenPair(user.ToTokenPayload(user.Type))
		if errTx != nil {
			ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenErrorGenerate, err)
			return errTx
		}

		res = user.ToAuthRes(token.AccessToken, token.RefreshToken)

		go u.userRepository.SetCacheByEmailAndType(ctx, user)

		return nil
	})

	return
}

func (u *usecase) setUserFromSignupReq(ctx *isession.Isession, req userdto.SignupReq) (user dao.User, err error) {
	var (
		pwd        string
		isVerified bool
	)

	if strings.ToLower(req.AuthType) == constant.Google {
		pwd = varis.Empty
		isVerified = true
	} else {
		isForbidden := u.isForbiddenDomain(req.UserType, req.Email)
		if isForbidden {
			err = constant.ErrorForbiddenDomain
			ctx.Error(isession.GetSource(), msgLog, varis.CodeUnauthorized, err)
			return
		}

		pwd, err = u.resource.Secret.GeneratePassword(req.Password)
		if err != nil {
			ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorGenerate, err)
			return
		}
	}

	user = dao.User{
		Email:              req.Email,
		Type:               req.UserType,
		Password:           pwd,
		Source:             ctx.Source,
		IsProfileCompleted: false,
		IsVerified:         isVerified,
		IsActive:           true,
	}

	return
}

func (u *usecase) setUserAccessFromSignupReq(userID, svcID uint64) (ua dao.UserAccess) {
	ua = dao.UserAccess{
		UserID:    userID,
		ServiceID: svcID,
		Role:      constant.DefaultRole,
		UpdatedBy: constant.CreatedBySystem,
		IsActive:  true,
	}

	return
}
