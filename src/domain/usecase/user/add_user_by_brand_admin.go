package user

import (
	"strings"

	"gorm.io/gorm"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/helper"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	userdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (u *usecase) AddUserByBrandAdmin(ctx *isession.Isession, req userdto.SignupReq) (userID uint64, err error) {
	req.Password = helper.GenerateRandomPassword(10)

	userReq, err := u.setUserFromOpSignupReq(ctx, req)
	if err != nil {
		return
	}

	err = u.resource.DB.Transaction(func(tx *gorm.DB) error {
		u.resource.SetDBTrx(tx)
		defer u.resource.ClearDBTrx()

		user, errTx := u.userRepository.Insert(ctx, userReq)
		if errTx != nil {
			return errTx
		}

		errTx = u.uaRepository.Insert(ctx, u.setUserAccessFromSignupReq(user.ID, 1))
		if errTx != nil {
			return errTx
		}

		userID = user.ID
		go u.sendMailOperatorSignupSuccess(ctx, user, req.Password)

		return nil
	})

	return
}

func (u *usecase) setUserFromOpSignupReq(ctx *isession.Isession, req userdto.SignupReq) (user dao.User, err error) {
	isForbidden := u.isForbiddenDomain(req.UserType, req.Email)
	if isForbidden {
		err = constant.ErrorForbiddenDomain
		ctx.Error(isession.GetSource(), msgLog, varis.CodeUnauthorized, err)
		return
	}

	pwd, err := u.resource.Secret.GeneratePassword(req.Password)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodePasswordErrorGenerate, err)
		return
	}

	user = dao.User{
		Email:              req.Email,
		Type:               req.UserType,
		Password:           pwd,
		Source:             ctx.Source,
		IsProfileCompleted: true,
		IsVerified:         true,
		IsActive:           true,
	}

	return
}

func (u *usecase) sendMailOperatorSignupSuccess(ctx *isession.Isession, user dao.User, randPass string) {
	emails := strings.Split(user.Email, "@")
	bodyMail := []*grpc.KeyValue{
		{Key: "name", Value: emails[0]},
		{Key: "email", Value: user.Email},
		{Key: "generated_password", Value: randPass},
	}

	mailReq := &grpc.SendMailRequest{
		RequestID: ctx.XID,
		Content: &grpc.Mail{
			To:   user.Email,
			Type: constant.TypeOperatorSignup,
			Body: bodyMail,
		},
	}

	u.puputanGRPC.SendMail(ctx, mailReq)

}
