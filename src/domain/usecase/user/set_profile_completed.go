package user

import (
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (u *usecase) SetProfileCompleted(ctx *isession.Isession, userID uint64) (err error) {
	userRes, err := u.userRepository.GetByID(ctx, userID)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorUserNotFound.Error()
		return
	}

	err = u.userRepository.Update(ctx, userRes, dao.User{IsProfileCompleted: true})
	if err != nil {
		return
	}

	go u.userRepository.DeleteCacheByEmailAndType(ctx, userRes.Email, userRes.Type)

	return
}
