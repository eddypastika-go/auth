package user

import (
	"strings"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
)

func (u *usecase) isForbiddenDomain(userType, email string) bool {
	domains := strings.Split(u.resource.Config.ForbiddenDomain, ",")

	switch userType {
	case constant.Brand, constant.Client:
		for _, domain := range domains {
			if strings.Contains(email, domain) {
				return true
			}
		}
	default:
		return false
	}

	return false
}
