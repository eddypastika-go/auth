package user

import (
	puputanGRPC "gitlab.com/eddypastika-go/auth/src/domain/repository/grpc/notify"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/utilities/entitis"

	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/service"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/useraccess"
	userdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "User Usecase"

type (
	Usecase interface {
		Signup(ctx *isession.Isession, req userdto.SignupReq, code, secret string) (res userdto.AuthRes, err error)
		Signin(ctx *isession.Isession, req userdto.SigninReq, code, secret string) (res userdto.AuthRes, err error)
		ChangePassword(ctx *isession.Isession, req userdto.ChangePasswordReq, ul entitis.TokenPayload) (err error)
		SetProfileCompleted(ctx *isession.Isession, userID uint64) (err error)
		AddUserByBrandAdmin(ctx *isession.Isession, req userdto.SignupReq) (userID uint64, err error)
	}
	usecase struct {
		userRepository user.Repository
		uaRepository   useraccess.Repository
		svcRepository  service.Repository
		puputanGRPC    puputanGRPC.Repository
		resource       *model.Resource
	}
)

func New(
	userRepository user.Repository,
	uaRepository useraccess.Repository,
	svcRepository service.Repository,
	puputanGRPC puputanGRPC.Repository,
	resource *model.Resource,
) Usecase {
	return &usecase{
		userRepository: userRepository,
		uaRepository:   uaRepository,
		svcRepository:  svcRepository,
		puputanGRPC:    puputanGRPC,
		resource:       resource,
	}
}
