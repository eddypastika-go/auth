package useraccess

import (
	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"
	uadto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/useraccess"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (u *usecase) Create(ctx *isession.Isession, req uadto.CreateReq, ul entitis.TokenPayload) (err error) {
	_, err = u.userRepository.GetByID(ctx, req.UserID)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorUserNotFound.Error()
		return constant.ErrorUserNotFound
	}

	_, err = u.svcRepository.GetByID(ctx, req.ServiceID)
	if err != nil {
		ctx.ErrorMessage = constant.ErrorServiceNotFound.Error()
		return constant.ErrorServiceNotFound
	}

	err = u.uaRepository.Insert(ctx, u.setUserAccessFromCreateReq(req, ul))
	if err != nil {
		return
	}

	return
}

func (u *usecase) setUserAccessFromCreateReq(req uadto.CreateReq, ul entitis.TokenPayload) (ua dao.UserAccess) {
	ua = dao.UserAccess{
		UserID:    req.UserID,
		ServiceID: req.ServiceID,
		Role:      req.Role,
		UpdatedBy: ul.Email,
		IsActive:  true,
	}

	return
}
