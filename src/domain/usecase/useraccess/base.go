package useraccess

import (
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/service"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/user"
	"gitlab.com/eddypastika-go/auth/src/domain/repository/persistent/useraccess"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	uadto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/useraccess"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "User Access Usecase"

type (
	Usecase interface {
		Create(ctx *isession.Isession, req uadto.CreateReq, userLogin entitis.TokenPayload) (err error)
	}

	usecase struct {
		userRepository user.Repository
		uaRepository   useraccess.Repository
		svcRepository  service.Repository
		resource       *model.Resource
	}
)

func New(
	userRepository user.Repository,
	uaRepository useraccess.Repository,
	svcRepository service.Repository,
	resource *model.Resource,
) Usecase {
	return &usecase{
		userRepository: userRepository,
		uaRepository:   uaRepository,
		svcRepository:  svcRepository,
		resource:       resource,
	}
}
