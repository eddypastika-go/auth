package token

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/eddypastika-go/auth/src/shared/config"
	"gitlab.com/eddypastika-go/utilities/entitis"
	"gitlab.com/eddypastika-go/utilities/isession"
)

var msgLog = "Token Auth"

type GoogleClaims struct {
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	FirstName     string `json:"given_name"`
	LastName      string `json:"family_name"`
	jwt.StandardClaims
}

type Token interface {
	GenerateTokenPair(p entitis.TokenPayload) (*entitis.TokenPair, error)
	ValidateGoogleJWT(ctx *isession.Isession, idToken string) (*GoogleClaims, error)
}

type token struct {
	config *config.Config
}

func New(config *config.Config) Token {
	return &token{config: config}
}
