package token

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/utilities/isession"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"

	"github.com/dgrijalva/jwt-go"
)

const (
	kid      = "kid"
	iss1     = "accounts.google.com"
	iss2     = "https://accounts.google.com"
	certsURL = "https://www.googleapis.com/oauth2/v1/certs"
)

func (t *token) ValidateGoogleJWT(ctx *isession.Isession, tokenString string) (claims *GoogleClaims, err error) {
	gToken, err := jwt.ParseWithClaims(tokenString, &GoogleClaims{}, t.googleKeyFunc)
	if err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenInvalid, err)
		return
	}

	claims, ok := gToken.Claims.(*GoogleClaims)
	if !ok {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenInvalid, constant.ErrorGoogleInvalidToken)
		return
	}

	if claims.Issuer != iss1 && claims.Issuer != iss2 {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenInvalid, constant.ErrorGoogleIssuer)
		return
	}

	// todo: check client id
	//if claims.Audience != "YOUR_CLIENT_ID_HERE" {
	//	return GoogleClaims{}, errors.New("aud is invalid")
	//}

	if claims.ExpiresAt < time.Now().Unix() {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeTokenInvalid, constant.ErrorGoogleExpiredToken)
		return
	}

	return claims, nil
}

func getGooglePublicKey(keyID string) (string, error) {
	resp, err := http.Get(certsURL)
	if err != nil {
		return "", err
	}
	dat, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	myResp := map[string]string{}
	err = json.Unmarshal(dat, &myResp)
	if err != nil {
		return "", err
	}

	key, ok := myResp[keyID]
	if !ok {
		return "", errors.New("key not found")
	}

	return key, nil
}

func (t *token) googleKeyFunc(token *jwt.Token) (interface{}, error) {
	pem, err := getGooglePublicKey(fmt.Sprintf("%s", token.Header[kid]))
	if err != nil {
		return nil, err
	}
	key, err := jwt.ParseRSAPublicKeyFromPEM([]byte(pem))
	if err != nil {
		return nil, err
	}
	return key, nil
}
