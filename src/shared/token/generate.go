package token

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/eddypastika-go/utilities/entitis"
)

func (t *token) GenerateTokenPair(p entitis.TokenPayload) (*entitis.TokenPair, error) {
	var err error
	secret := []byte(t.config.TokenSecret)

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": map[string]interface{}{
			"id":    p.ID,
			"email": p.Email,
			"type":  p.Type,
		},
		"exp": uint64(time.Now().AddDate(0, 0, t.config.TokenExpiresInDay).Unix()),
	})

	at, err := accessToken.SignedString(secret)
	if err != nil {
		return nil, err
	}

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":   p.ID,
		"type": p.Type,
		"exp":  uint64(time.Now().AddDate(0, 0, t.config.TokenRefreshExpiresInDay).Unix()),
	})

	rt, err := refreshToken.SignedString(secret)
	if err != nil {
		return nil, err
	}

	return &entitis.TokenPair{
		AccessToken:  at,
		RefreshToken: rt,
	}, nil
}
