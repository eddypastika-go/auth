package redis

import (
	"fmt"
	"strings"

	"gitlab.com/eddypastika-go/auth/src/shared/config"
	"gitlab.com/eddypastika-go/utilities/cache"
	"gitlab.com/eddypastika-go/utilities/cache/redis"
)

func New(c *config.Config) cache.Cache {
	fmt.Println("============ Redis Start ==============")

	if len(c.RedisAddress) == 0 {
		panic("redis address is nil")
	}

	addr := strings.Split(c.RedisAddress, ",")
	redisCfg := redis.Config{
		Debug:      c.RedisDebugMode,
		Prefix:     c.RedisPrefix,
		Mode:       c.RedisMode,
		Addresses:  addr,
		MasterName: c.RedisMasterName,
	}

	if c.RedisPoolSize > 0 {
		redisCfg.PoolSize = c.RedisPoolSize
	}

	client, err := redis.New(redisCfg)
	if err != nil {
		panic(err)
	}

	return client
}
