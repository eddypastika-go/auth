package constant

import "errors"

const (
	StartApp        = "auth is service for handling authentication and authorization"
	AppCode         = "REN"
	CreatedBySystem = "system"
	DefaultRole     = "default"
)

// - user type
const (
	Internal = "internal"
	Brand    = "brand"
	Client   = "client"
)

// - auth type
const (
	Email  = "email"
	Google = "google"
)

// - email template type
const (
	TypeBrandSignupVerification       = "brand_signup_verification"
	TypeClientSignupVerification      = "client_signup_verification"
	TypeChangePasswordVerification    = "change_password"
	TypeForgotPasswordVerification    = "forgot_password"
	TypeChangeEmailVerification       = "change_email"
	TypeDeactivateAccountVerification = "deactivate_account"
	TypeOperatorSignup                = "operator_signup"
)

// - error
var (
	ErrorForbiddenDomain             = errors.New("invalid email domain")
	ErrorInvalidAuthType             = errors.New("invalid authentication type")
	ErrorInvalidID                   = errors.New("invalid ID")
	ErrorUserInactive                = errors.New("user inactive")
	ErrorUserNotFound                = errors.New("user not found")
	ErrorEmailAlreadyExist           = errors.New("email already exist")
	ErrorInvalidNewPassword          = errors.New("invalid new password")
	ErrorEmptyEmailOrType            = errors.New("email and user type cannot be empty")
	ErrorAccessDenied                = errors.New("access denied")
	ErrorVerificationNotFound        = errors.New("verification not found")
	ErrorInvalidVerificationTemplate = errors.New("invalid verification template")
	ErrorServiceNotFound             = errors.New("service not found")
	ErrorEndpointNotFound            = errors.New("endpoint not found")
	ErrorEndpointGroupNotFound       = errors.New("endpoint group not found")
	ErrorUserEmptyPassword           = errors.New("empty user password, please sign in with google and set your password")
	ErrorWrongPassword               = errors.New("wrong password")
	ErrorGoogleInvalidToken          = errors.New("invalid google token")
	ErrorGoogleIssuer                = errors.New("invalid google token issuer")
	ErrorGoogleExpiredToken          = errors.New("google token is expired")
	ErrorInvalidVerificationCode     = errors.New("invalid verification code")
	ErrorExpiredVerificationCode     = errors.New("verification code is expired")
)
