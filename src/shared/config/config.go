package config

import (
	"time"

	"gitlab.com/eddypastika-go/utilities/grpcis"

	"gitlab.com/eddypastika-go/utilities/iserver"

	"gitlab.com/eddypastika-go/utilities/cache/redis"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	// - server
	AppName            string        `envconfig:"APP_NAME" required:"true"`
	AppQuote           string        `envconfig:"APP_QUOTE" required:"true"`
	AppPort            int           `envconfig:"APP_PORT" default:"8090" required:"true"`
	AppPortGRPC        int           `envconfig:"APP_PORT_GRPC" default:"8091" required:"true"`
	AppVersion         string        `envconfig:"APP_VERSION" required:"true"`
	AppEnv             string        `envconfig:"APP_ENV" default:"development" required:"true"`
	GracefullyDuration time.Duration `envconfig:"GRACEFULLY_DURATION" default:"10s"`

	// - logger
	LogPath   string        `envconfig:"LOG_PATH" default:"logs/log" required:"true"`
	LogMaxAge time.Duration `envconfig:"LOG_MAX_AGE" default:"10m" required:"true"`
	LogStdout bool          `envconfig:"LOG_STDOUT" default:"false"`

	// - persistent
	DbDialect           string `envconfig:"DB_DIALECT" required:"true"`
	DbUser              string `envconfig:"DB_USER" required:"true"`
	DbPassword          string `envconfig:"DB_PASSWORD" required:"true"`
	DbHost              string `envconfig:"DB_HOST" required:"true"`
	DbPort              string `envconfig:"DB_PORT" required:"true"`
	DbName              string `envconfig:"DB_NAME" default:"denpasar" required:"true"`
	DbSchema            string `envconfig:"DB_SCHEMA" default:"auth" required:"true"`
	DbMaxIdleConnection int    `envconfig:"DB_MAX_IDLE_CONN" default:"10"`
	DbMaxOpenConnection int    `envconfig:"DB_MAX_OPEN_CONNECTION" default:"10"`
	DbDebugMode         bool   `envconfig:"DB_DEBUG_MODE" default:"false"`

	// - redis
	RedisAddress    string        `envconfig:"REDIS_ADDRESS" required:"true"`
	RedisMode       redis.Mode    `envconfig:"REDIS_MODE" default:"single" required:"true"`
	RedisPoolSize   int           `envconfig:"REDIS_POOL_SIZE" required:"true"`
	RedisPrefix     string        `envconfig:"REDIS_PREFIX" default:"auth" required:"true"`
	RedisMasterName string        `envconfig:"REDIS_MASTER_NAME" required:"true"`
	RedisDebugMode  bool          `envconfig:"REDIS_DEBUG_MODE" default:"false"`
	RedisDefaultTTL time.Duration `envconfig:"REDIS_DEFAULT_TTL" default:"10m" required:"true"`

	// - grpc
	GrpcTimeout       time.Duration `envconfig:"GRPC_TIMEOUT" required:"true" default:"10s"`
	GrpcDebug         bool          `envconfig:"GRPC_DEBUG" default:"false"`
	GrpcProxy         bool          `envconfig:"GRPC_PROXY" default:"false"`
	GrpcSkipTLS       bool          `envconfig:"GRPC_SKIP_TLS" default:"false"`
	GrpcProxyAddress  string        `envconfig:"GRPC_PROXY_ADDRESS"`
	NotifyGrpcAddress string        `envconfig:"NOTIFY_GRPC_ADDRESS" required:"true"`

	// - jwt token
	TokenSecret              string `envconfig:"TOKEN_SECRET" required:"true"`
	TokenExpiresInDay        int    `envconfig:"TOKEN_EXPIRES_IN_DAY" default:"5" required:"true"`
	TokenRefreshExpiresInDay int    `envconfig:"TOKEN_REFRESH_EXPIRES_IN_DAY" default:"10" required:"true"`

	// - general
	ForbiddenDomain string        `envconfig:"FORBIDDEN_DOMAIN" default:"eddypastika.com" required:"true"`
	NumberOTP       int           `envconfig:"NUMBER_OTP" default:"6" required:"true"`
	ExpiredOTP      time.Duration `envconfig:"EXPIRED_OTP" default:"3m" required:"true"`
}

func New() *Config {
	var cfg Config

	err := envconfig.Process("", &cfg)
	if err != nil {
		panic(err)
	}

	return &cfg
}

func (c *Config) ToIserverOpt() iserver.Option {
	return iserver.Option{
		Name:     c.AppName,
		Desc:     c.AppQuote,
		Version:  c.AppVersion,
		HTTPPort: c.AppPort,
		GRPCPort: c.AppPortGRPC,
	}
}

func (c *Config) ToGRPCOpt(addr string) grpcis.Options {
	return grpcis.Options{
		Address:      addr,
		Timeout:      c.GrpcTimeout,
		DebugMode:    c.GrpcDebug,
		WithProxy:    c.GrpcProxy,
		ProxyAddress: c.GrpcProxyAddress,
		SkipTLS:      c.GrpcSkipTLS,
	}
}
