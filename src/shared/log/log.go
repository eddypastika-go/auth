package log

import (
	"fmt"
	"os"

	"gitlab.com/eddypastika-go/auth/src/shared/config"
	"gitlab.com/eddypastika-go/utilities/logger"
)

func New(cfg *config.Config) logger.Logger {
	// Get root path
	dir, err := os.Getwd()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "error get parent path %s", err.Error())
		panic(err)
	}

	opt := logger.OptionsFile{
		Stdout:       cfg.LogStdout,
		FileLocation: fmt.Sprintf("%s/%s", dir, cfg.LogPath),
		FileMaxAge:   cfg.LogMaxAge,
	}

	return logger.SetupLoggerFile(cfg.AppName, &opt)
}
