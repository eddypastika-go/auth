package service

type CreateReq struct {
	Name       string `json:"name" validate:"required"`
	IsExternal bool   `json:"is_external"`
}
