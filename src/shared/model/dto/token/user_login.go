package token

type Payload struct {
	ID    uint64
	Email string
	Type  string
}
