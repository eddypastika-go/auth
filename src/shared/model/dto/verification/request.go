package verification

type (
	SendReq struct {
		Email    string `json:"email" validate:"required"`
		UserType string `json:"user_type" validate:"required"`
		Template string `json:"template" validate:"required"`
	}

	ConfirmReq struct {
		Email    string `json:"email" validate:"required"`
		UserType string `json:"user_type" validate:"required"`
		Template string `json:"template" validate:"required"`
		Code     string `json:"code" validate:"required"`
	}
)
