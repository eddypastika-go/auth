package useraccess

type CreateReq struct {
	UserID    uint64 `json:"user_id" validate:"required"`
	ServiceID uint64 `json:"service_id" validate:"required"`
	Role      string `json:"role" validate:"required"`
}
