package user

type AuthRes struct {
	ID                 uint64 `json:"id"`
	Email              string `json:"email"`
	Type               string `json:"type"`
	AccessToken        string `json:"access_token"`
	RefreshToken       string `json:"refresh_token"`
	IsProfileCompleted bool   `json:"is_profile_completed"`
	IsPasswordSet      bool   `json:"is_password_set"`
	IsVerified         bool   `json:"is_verified"`
}
