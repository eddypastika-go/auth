package user

type SignupReq struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=8"`
	UserType string `json:"user_type" validate:"required"`
	AuthType string `json:"auth_type"`
}

type SigninReq struct {
	Email    string `json:"email" validate:"required,email"`
	IDToken  string `json:"id_token" validate:"required"`
	Password string `json:"password" validate:"required,min=8"`
	UserType string `json:"user_type" validate:"required"`
	AuthType string `json:"auth_type" validate:"required"`
}

type EmailSignin struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=8"`
	UserType string `json:"user_type" validate:"required"`
}

type GoogleSignin struct {
	IDToken  string `json:"id_token" validate:"required"`
	UserType string `json:"user_type" validate:"required"`
}

func (si *SigninReq) ToEmailSignin() EmailSignin {
	return EmailSignin{
		Email:    si.Email,
		Password: si.Password,
		UserType: si.UserType,
	}
}

func (si *SigninReq) ToGoogleSignin() GoogleSignin {
	return GoogleSignin{
		IDToken:  si.IDToken,
		UserType: si.UserType,
	}
}

type ChangePasswordReq struct {
	OldPassword        string `json:"old_password" validate:"required,min=8"`
	NewPassword        string `json:"new_password" validate:"required,min=8,nefield=OldPassword"`
	ConfirmNewPassword string `json:"confirm_new_password" validate:"required,min=8,eqfield=NewPassword"`
}
