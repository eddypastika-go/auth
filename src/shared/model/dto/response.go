package dto

// Response ...
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// DefaultResponse ...
type DefaultResponse struct {
	Data interface{} `json:"data"`
	Response
}
