package health

import (
	"time"

	"gitlab.com/eddypastika-go/utilities/varis"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dto"
)

type DataResponse struct {
	TS     time.Time     `json:"ts"`
	Pid    int           `json:"pid"`
	Uptime time.Duration `json:"uptime"`
	Memory memoryUsage   `json:"memory"`
	Status string        `json:"status"`
}

type memoryUsage struct {
	Alloc      uint64 `json:"alloc"`
	TotalAlloc uint64 `json:"totalAlloc"`
	Sys        uint64 `json:"sys"`
	HeapAlloc  uint64 `json:"heapAlloc"`
	HeapSys    uint64 `json:"heapSys"`
	NumGC      uint32 `json:"numGC"`
}

func (data *DataResponse) ToResponse() dto.DefaultResponse {
	return dto.DefaultResponse{
		Data: data,
		Response: dto.Response{
			Status:  varis.CodeSuccess,
			Message: varis.MsgSuccess,
		},
	}
}
