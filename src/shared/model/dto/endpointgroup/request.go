package endpointgroup

type CreateReq struct {
	EndpointID uint64 `json:"endpoint_id" validate:"required"`
	ServiceID  uint64 `json:"service_id" validate:"required"`
	Role       string `json:"role" validate:"required"`
}
