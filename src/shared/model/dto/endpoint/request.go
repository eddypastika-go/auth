package endpoint

type UpsertReq struct {
	Path        string `json:"path" validate:"required"`
	Method      string `json:"method" validate:"required"`
	Type        string `json:"type" validate:"required"`
	ServiceName string `json:"service_name" validate:"required"`
}
