package dao

type GoogleUserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	FamilyName    string `json:"family_name"`
	GivenName     string `json:"given_name"`
	Picture       string `json:"picture"`
	EmailVerified bool   `json:"email_verified"`
	Locale        string `json:"locale"`
	Hd            string `json:"hd"`
}
