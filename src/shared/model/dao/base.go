package dao

import (
	"time"

	"gorm.io/gorm"
)

type Base struct {
	CreatedAt *time.Time     `sql:"DEFAULT:now()" json:"-"`
	UpdatedAt *time.Time     `sql:"DEFAULT:now()" json:"-"`
	DeletedAt gorm.DeletedAt `sql:"index" json:"-"`
}
