package dao

type Service struct {
	Base
	ID         uint64 `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	Name       string `gorm:"type:varchar(50);not null" sql:"unique_index:idx_unique_name" json:"name"`
	Code       string `gorm:"type:varchar(50);not null" sql:"unique_index:idx_unique_code" json:"code"`
	Secret     string `gorm:"type:varchar(100);not null" sql:"unique_index:idx_unique_secret" json:"secret"`
	UpdatedBy  string `gorm:"type:varchar(100);not null" json:"updated_by"`
	IsExternal bool   `gorm:"type:bool" json:"is_external"`
	IsActive   bool   `gorm:"type:bool" json:"is_active"`
}

func (s *Service) TableName() string {
	return "services"
}
