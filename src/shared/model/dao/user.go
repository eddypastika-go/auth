package dao

import (
	"strings"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/entitis"
)

type User struct {
	Base
	ID                 uint64 `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	Email              string `gorm:"type:varchar(255);not null" sql:"unique_index:idx_unique_email_type" json:"email"`
	Password           string `gorm:"type:varchar(255);not null" json:"password"`
	Type               string `gorm:"type:varchar(255);not null" sql:"unique_index:idx_unique_email_type" json:"user_type"`
	Source             string `gorm:"type:varchar(20);default:'webapp'" json:"source"`
	IsProfileCompleted bool   `gorm:"type:bool" json:"is_profile_completed"`
	IsVerified         bool   `gorm:"type:bool" json:"is_verified"`
	IsActive           bool   `gorm:"type:bool" json:"is_active"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) ToAuthRes(aToken, rToken string) user.AuthRes {
	isPasswordSet := true
	if u.Password == "" {
		isPasswordSet = false
	}

	return user.AuthRes{
		ID:                 u.ID,
		Email:              u.Email,
		Type:               strings.ToLower(u.Type),
		AccessToken:        aToken,
		RefreshToken:       rToken,
		IsProfileCompleted: u.IsProfileCompleted,
		IsPasswordSet:      isPasswordSet,
		IsVerified:         u.IsVerified,
	}
}

func (u *User) ToTokenPayload(userType string) entitis.TokenPayload {
	return entitis.TokenPayload{
		ID:    u.ID,
		Email: u.Email,
		Type:  strings.ToLower(userType),
	}
}
