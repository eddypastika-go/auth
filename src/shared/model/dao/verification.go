package dao

import (
	"strings"
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
)

type Verification struct {
	Base
	ID         uint64    `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	Email      string    `gorm:"type:varchar(255);not null" json:"email"`
	UserType   string    `gorm:"type:varchar(25);not null" json:"user_type"`
	Code       string    `gorm:"type:varchar(6);not null" json:"-"`
	Template   string    `gorm:"type:varchar(100);not null" json:"-"`
	MaxCount   int       `gorm:"type:int;default:3" json:"-"`
	ExpiredAt  time.Time `sql:"DEFAULT:now()+interval '3' minute" json:"-"`
	IsVerified bool      `gorm:"type:bool" json:"-"`
}

func (v *Verification) TableName() string {
	return "verifications"
}

func (v *Verification) ToDefaultSendParam() (notifParam map[string]string) {
	emails := strings.Split(v.Email, "@")
	notifParam = map[string]string{
		"name": emails[0],
		"code": v.Code,
	}

	return
}

func (v *Verification) ToChangeEmailSendParam() (notifParam map[string]string) {
	notifParam = v.ToDefaultSendParam()
	notifParam["user_type"] = v.UserType

	return
}

func (v *Verification) ToSendMailVerificationCode(reqID string, notifParam map[string]string, verifyType string) (res *grpc.SendMailRequest) {
	if notifParam == nil {
		emails := strings.Split(v.Email, "@")
		notifParam = map[string]string{
			"name": emails[0],
			"code": v.Code,
		}
	}

	body := make([]*grpc.KeyValue, len(notifParam))
	for key, val := range notifParam {
		par := &grpc.KeyValue{
			Key:   key,
			Value: val,
		}
		body = append(body, par)
	}

	res = &grpc.SendMailRequest{
		RequestID: reqID,
		Content: &grpc.Mail{
			To:   v.Email,
			Type: v.Template + "_" + verifyType,
			Body: body,
		},
	}

	return
}
