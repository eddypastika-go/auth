package dao

type UserAccess struct {
	Base
	ID        uint64 `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	UserID    uint64 `gorm:"type:integer;not null" json:"user_id"`
	ServiceID uint64 `gorm:"type:integer;not null" json:"service_id"`
	Role      string `gorm:"type:varchar(100);not null;default:'default'" json:"role"`
	UpdatedBy string `gorm:"type:varchar(100);not null" json:"updated_by"`
	IsActive  bool   `gorm:"type:bool" json:"is_active"`
}

func (u *UserAccess) TableName() string {
	return "user_accesses"
}
