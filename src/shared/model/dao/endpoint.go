package dao

type Endpoint struct {
	Base
	ID          uint64 `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	Path        string `gorm:"type:varchar(100);not null;check:path <> '';uniqueIndex:idx_path_method" json:"path"`
	Method      string `gorm:"type:varchar(10);not null;check:method <> '';uniqueIndex:idx_path_method" json:"method"`
	Type        string `gorm:"type:varchar(20);not null;check:type <> ''" json:"type"`
	ServiceName string `gorm:"type:varchar(100);not null;check:service_name <> ''" json:"service_name"`
	UpdatedBy   string `gorm:"type:varchar(100);not null;default:'system'" json:"updated_by"`
	IsActive    bool   `gorm:"type:bool" json:"is_active"`
}

func (e *Endpoint) TableName() string {
	return "endpoints"
}
