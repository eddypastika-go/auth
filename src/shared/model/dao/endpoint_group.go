package dao

type EndpointGroup struct {
	Base
	ID         uint64 `gorm:"primary_key;AUTO_INCREMENT" json:"id"`
	ServiceID  uint64 `gorm:"type:integer;not null" json:"service_id"`
	EndpointID uint64 `gorm:"type:integer;not null" json:"endpoint_id"`
	Role       string `gorm:"type:varchar(100);not null;default:'default'" json:"role"`
	UpdatedBy  string `gorm:"type:varchar(100);not null" json:"updated_by"`
}

func (e *EndpointGroup) TableName() string {
	return "endpoint_groups"
}
