package model

import (
	"gitlab.com/eddypastika-go/auth/src/shared/config"
	"gitlab.com/eddypastika-go/auth/src/shared/database"
	tokenauth "gitlab.com/eddypastika-go/auth/src/shared/token"
	"gitlab.com/eddypastika-go/utilities/cache"
	"gitlab.com/eddypastika-go/utilities/logger"
	"gitlab.com/eddypastika-go/utilities/secret"
	"gitlab.com/eddypastika-go/utilities/token"
	"gorm.io/gorm"
)

type Resource struct {
	Config    *config.Config
	DB        *database.Database
	DBTrx     *database.Database
	Redis     cache.Cache
	Logger    logger.Logger
	TokenAuth tokenauth.Token
	TokenUtil token.Token
	Secret    secret.Secret
}

func (r *Resource) GetDB() (db *gorm.DB) {
	db = r.DB.DB
	if r.DBTrx != nil {
		db = r.DBTrx.DB
	}

	return
}

func (r *Resource) SetDBTrx(tx *gorm.DB) {
	r.DBTrx = database.NewTrx(tx)
}

func (r *Resource) ClearDBTrx() {
	r.DBTrx = nil
}
