package database

import (
	"fmt"
	"time"

	"gorm.io/gorm/logger"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dao"

	_ "github.com/lib/pq"
	"gitlab.com/eddypastika-go/auth/src/shared/config"
)

type Database struct {
	*gorm.DB
}

func New(config *config.Config) *Database {
	fmt.Println("============ Database Start ==============")

	dbConfig := &gorm.Config{}
	if config.DbDebugMode {
		dbConfig = &gorm.Config{Logger: logger.Default.LogMode(logger.Info)}
	}

	dsn := fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%v sslmode=disable search_path=%s",
		config.DbUser, config.DbPassword, config.DbName, config.DbHost, config.DbPort, config.DbSchema,
	)
	db, err := gorm.Open(postgres.Open(dsn), dbConfig)
	if err != nil {
		fmt.Println("failed to connect database")
		panic(err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		fmt.Println("failed to connect database")
		panic(err)
	}

	err = sqlDB.Ping()
	if err != nil {
		fmt.Println("failed to ping")
		panic(err)
	}

	sqlDB.SetMaxIdleConns(config.DbMaxIdleConnection)
	sqlDB.SetMaxOpenConns(config.DbMaxOpenConnection)
	sqlDB.SetConnMaxLifetime(time.Hour)

	database := &Database{db}

	if config.DbDebugMode {
		_ = db.AutoMigrate(&dao.Endpoint{})
		_ = db.AutoMigrate(&dao.EndpointGroup{})
		_ = db.AutoMigrate(&dao.Service{})
		_ = db.AutoMigrate(&dao.User{})
		_ = db.AutoMigrate(&dao.UserAccess{})
		_ = db.AutoMigrate(&dao.Verification{})
		seedTestData(database)
	}

	return database
}

func NewTrx(tx *gorm.DB) *Database {
	return &Database{tx}
}

func seedTestData(db *Database) {
	contentSvc := &dao.Service{
		Name:       "tested",
		Code:       "ngurahrai",
		Secret:     "ngurahrai",
		UpdatedBy:  "tested",
		IsExternal: false,
		IsActive:   true,
	}

	err := db.Where(&contentSvc).First(&dao.Service{}).Error
	if err != nil {
		err = db.Save(contentSvc).Error
		if err != nil {
			fmt.Println("error seeding")
		}
	}
}
