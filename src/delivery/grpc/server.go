package grpc

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"

	"gitlab.com/eddypastika-go/auth/src"

	"gitlab.com/eddypastika-go/utilities/grpcis"

	"gitlab.com/eddypastika-go/utilities/mware"

	"google.golang.org/grpc/reflection"

	"github.com/spf13/cast"
	grpcmodel "gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"google.golang.org/grpc"
)

func Start(ctx context.Context, container *src.Container) error {
	cfg := container.Config

	port := cast.ToString(cfg.AppPortGRPC)
	listen, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}

	// register service
	server := grpc.NewServer(
		grpc.UnaryInterceptor(mware.GrpcInterceptor(container.Logger, cfg.AppName, cfg.AppVersion, cfg.AppPortGRPC)),
	)
	reflection.Register(server)

	// setup handler
	handler := NewHandler(container)
	grpcis.RegisterHealthServer(server)

	//health.RegisterHealthServer(server)
	grpcmodel.RegisterEndpointHandlerServer(server, handler.EndpointHandler)
	grpcmodel.RegisterUserHandlerServer(server, handler.UserHandler)

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
			log.Println("shutting down gRPC server")

			server.GracefulStop()

			<-ctx.Done()
		}
	}()

	// start gRPC server
	return server.Serve(listen)
}
