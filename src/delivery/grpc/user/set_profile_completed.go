package user

import (
	"context"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/common/typeconv"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/mware"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (h *Handler) SetProfileCompleted(c context.Context, req *grpc.CompletedProfileRequest) (res *grpc.DefaultResponse, err error) {
	ctx := isession.SetGrpcCtx(c, typeconv.ConvertUint64ToString(req.UserID), constant.AppCode, req)

	err = h.userUsecase.SetProfileCompleted(ctx, req.UserID)
	message := varis.MsgSuccess
	ctx.ResponseCode = varis.CodeSuccess
	if err != nil {
		ctx.ResponseCode, message = mware.ErrorHandler(ctx, isession.GetSource(), msgLog, err)
	}

	res = &grpc.DefaultResponse{
		Status:  ctx.ResponseCode,
		Message: message,
	}

	ctx.LOGIS(msgLog, typeconv.ToJSONIndent(res))
	return
}
