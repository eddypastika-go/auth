package user

import (
	"context"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/common/typeconv"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/mware"
	"gitlab.com/eddypastika-go/utilities/varis"
)

func (h *Handler) AddUserByBrandAdmin(c context.Context, req *grpc.AddUserByBrandAdminReq) (res *grpc.AddUserByBrandAdminResponse, err error) {
	ctx := isession.SetGrpcCtx(c, req.RequestID, constant.AppCode, req)

	userID, err := h.userUsecase.AddUserByBrandAdmin(ctx, setRequest(req))
	message := varis.MsgSuccess
	ctx.ResponseCode = varis.CodeSuccess
	if err != nil {
		ctx.ResponseCode, message = mware.ErrorHandler(ctx, isession.GetSource(), msgLog, err)
	}

	res = &grpc.AddUserByBrandAdminResponse{
		Status:  ctx.ResponseCode,
		Message: message,
		UserID:  userID,
	}

	ctx.LOGIS(msgLog, typeconv.ToJSONIndent(res))
	return
}

func setRequest(req *grpc.AddUserByBrandAdminReq) (request user.SignupReq) {
	request = user.SignupReq{
		Email:    req.Email,
		UserType: "brand",
		AuthType: "email",
	}

	return
}
