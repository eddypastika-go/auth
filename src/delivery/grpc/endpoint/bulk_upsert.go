package endpoint

import (
	"context"

	"gitlab.com/eddypastika-go/utilities/common/typeconv"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"

	"gitlab.com/eddypastika-go/utilities/mware"

	"gitlab.com/eddypastika-go/utilities/varis"

	endpointdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpoint"

	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) BulkUpsert(c context.Context, req *grpc.BulkUpsertRequest) (res *grpc.BulkUpsertResponse, err error) {
	ctx := isession.SetGrpcCtx(c, req.RequestID, constant.AppCode, req)

	er := h.endpointUsecase.BulkUpsert(ctx, setRequest(req))
	message := varis.MsgSuccess
	ctx.ResponseCode = varis.CodeSuccess
	if er != nil {
		ctx.ResponseCode, message = mware.ErrorHandler(ctx, isession.GetSource(), msgLog, er)
	}

	res = &grpc.BulkUpsertResponse{
		Status:  ctx.ResponseCode,
		Message: message,
	}

	ctx.LOGIS(msgLog, typeconv.ToJSONIndent(res))
	return
}

func setRequest(req *grpc.BulkUpsertRequest) []endpointdto.UpsertReq {
	request := make([]endpointdto.UpsertReq, len(req.Items))
	for i, v := range req.Items {
		request[i] = endpointdto.UpsertReq{
			Path:        v.Path,
			Method:      v.Method,
			Type:        v.Type,
			ServiceName: v.ServiceName,
		}
	}
	return request
}
