package grpc

import (
	"gitlab.com/eddypastika-go/auth/src"
	"gitlab.com/eddypastika-go/auth/src/delivery/grpc/endpoint"
	"gitlab.com/eddypastika-go/auth/src/delivery/grpc/user"
	"gitlab.com/eddypastika-go/auth/src/shared/model/grpc"
)

type Handler struct {
	EndpointHandler grpc.EndpointHandlerServer
	UserHandler     grpc.UserHandlerServer
}

func NewHandler(container *src.Container) *Handler {
	return &Handler{
		EndpointHandler: endpoint.New(container.EndpointUsecase, container.Resource),
		UserHandler:     user.New(container.UserUsecase, container.Resource),
	}
}
