package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
	"gitlab.com/eddypastika-go/utilities/iserver"
	"gitlab.com/eddypastika-go/utilities/mware"
	"gitlab.com/eddypastika-go/utilities/varis"
)

// SetupRoutes is for register all endpoints
func SetupRoutes(e *echo.Echo, handler *Handler, resource *model.Resource) {
	iserver.GetAllRoutes(e, resource.Config.AppName)
	v1 := e.Group("/v1")
	authMe := v1.Group("/me")

	// - Health
	e.GET("/", handler.health.Hello).Name = varis.EndpointMancanegara
	e.GET("/health", handler.health.Health).Name = varis.EndpointMancanegara
	e.GET("/ping", handler.health.Ping).Name = varis.EndpointMancanegara

	// - Endpoint
	endpoint := v1.Group("/endpoints", mware.DecodeToken(resource.Logger, resource.TokenUtil))
	endpoint.POST("/bulk-upsert", handler.endpoint.BulkUpsert).Name = varis.EndpointLokal
	endpoint.DELETE("/:id", handler.endpoint.DeleteByID).Name = varis.EndpointLokal
	endpoint.GET("", handler.endpoint.GetAllInPagination).Name = varis.EndpointLokal

	// - Endpoint Group
	endpointGroups := v1.Group("/endpoint-groups", mware.DecodeToken(resource.Logger, resource.TokenUtil))
	endpointGroups.POST("", handler.endpointGroup.Create).Name = varis.EndpointLokal
	endpointGroups.DELETE("/:id", handler.endpointGroup.DeleteByID).Name = varis.EndpointLokal

	// - Service
	service := v1.Group("/services", mware.DecodeToken(resource.Logger, resource.TokenUtil))
	service.POST("", handler.service.Create).Name = varis.EndpointLokal

	// - Auth User
	user := v1.Group("/users")
	user.POST("/signup", handler.user.Signup).Name = varis.EndpointDomestik
	user.POST("/signin", handler.user.Signin).Name = varis.EndpointDomestik

	userMe := authMe.Group("/users", mware.DecodeToken(resource.Logger, resource.TokenUtil))
	userMe.PATCH("/change-password", handler.user.ChangePassword).Name = varis.EndpointLokal

	// - User Access
	userAccess := v1.Group("/user-accesses", mware.DecodeToken(resource.Logger, resource.TokenUtil))
	userAccess.POST("", handler.userAccess.Create).Name = varis.EndpointLokal

	// - Verification
	verify := v1.Group("/verifications")
	verify.POST("/send", handler.verification.Send).Name = varis.EndpointDomestik
	verify.POST("/confirm", handler.verification.Confirm).Name = varis.EndpointDomestik

}
