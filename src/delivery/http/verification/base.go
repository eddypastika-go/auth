package verification

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/verification"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "Verification HTTP Delivery"

type Handler struct {
	verifyUsecase verification.Usecase
	resource      *model.Resource
}

func New(verifUsecase verification.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		verifyUsecase: verifUsecase,
		resource:      resource,
	}
}
