package verification

import (
	verifdto "gitlab.com/eddypastika-go/auth/src/shared/model/dto/verification"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Confirm(c echo.Context) error {
	var (
		req verifdto.ConfirmReq
		ctx = isession.NewFromEcho(c)
	)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	err := h.verifyUsecase.Confirm(ctx, req)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
