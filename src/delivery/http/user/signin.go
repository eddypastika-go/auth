package user

import (
	"gitlab.com/eddypastika-go/utilities/varis"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Signin(c echo.Context) error {
	var req user.SigninReq

	ctx := isession.NewFromEcho(c)
	code, secret := ctx.GetServiceCredential()

	if ctx.ErrorMessage != varis.Empty {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeInvalidContext, varis.ErrorInvalidContext)
		return ctx.JSONError()
	}

	// not using `ctx.ProcessRequest` because no need validation here
	if err := c.Bind(&req); err != nil {
		ctx.Error(isession.GetSource(), msgLog, varis.CodeBindError, err)
		return ctx.JSONError()
	}

	res, err := h.userUsecase.Signin(ctx, req, code, secret)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONResponse(res)
}
