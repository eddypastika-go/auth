package user

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Signup(c echo.Context) error {
	var req user.SignupReq

	ctx := isession.NewFromEcho(c)
	code, secret := ctx.GetServiceCredential()

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	res, err := h.userUsecase.Signup(ctx, req, code, secret)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONResponse(res)
}
