package user

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/user"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) ChangePassword(c echo.Context) error {
	var req user.ChangePasswordReq
	ctx := isession.NewFromEcho(c)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	userLogin, err := ctx.GetUserLogin()
	if err != nil {
		return ctx.JSONError()
	}

	err = h.userUsecase.ChangePassword(ctx, req, userLogin)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
