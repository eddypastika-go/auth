package user

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/user"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "User HTTP Delivery"

type Handler struct {
	userUsecase user.Usecase
	resource    *model.Resource
}

func New(userUsecase user.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		userUsecase: userUsecase,
		resource:    resource,
	}
}
