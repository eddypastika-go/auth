package endpoint

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/endpoint"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "Endpoint HTTP Delivery"

type Handler struct {
	endpointUsecase endpoint.Usecase
	resource        *model.Resource
}

func New(endpointUsecase endpoint.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		endpointUsecase: endpointUsecase,
		resource:        resource,
	}
}
