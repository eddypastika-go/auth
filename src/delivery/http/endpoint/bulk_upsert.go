package endpoint

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpoint"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) BulkUpsert(c echo.Context) error {
	var (
		req []endpoint.UpsertReq
		ctx = isession.NewFromEcho(c)
	)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	if err := h.endpointUsecase.BulkUpsert(ctx, req); err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
