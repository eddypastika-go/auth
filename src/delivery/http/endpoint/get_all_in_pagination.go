package endpoint

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/isession"
	"gitlab.com/eddypastika-go/utilities/queris"
)

func (h *Handler) GetAllInPagination(c echo.Context) error {
	var (
		ctx  = isession.NewFromEcho(c)
		page queris.Paging
	)

	res, err := h.endpointUsecase.GetAllInPagination(ctx, page.FromContext(c))
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONResponse(res)
}
