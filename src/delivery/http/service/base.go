package service

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/service"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "Service HTTP Delivery"

type Handler struct {
	serviceUsecase service.Usecase
	resource       *model.Resource
}

func New(serviceUsecase service.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		serviceUsecase: serviceUsecase,
		resource:       resource,
	}
}
