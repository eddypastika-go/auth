package service

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/service"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Create(c echo.Context) error {
	var (
		req service.CreateReq
		ctx = isession.NewFromEcho(c)
	)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	userLogin, err := ctx.GetUserLogin()
	if err != nil {
		return ctx.JSONError()
	}

	err = h.serviceUsecase.Create(ctx, req, userLogin)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
