package endpointgroup

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/endpointgroup"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Create(c echo.Context) error {
	var (
		req endpointgroup.CreateReq
		ctx = isession.NewFromEcho(c)
	)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	userLogin, err := ctx.GetUserLogin()
	if err != nil {
		return ctx.JSONError()
	}

	if err = h.endpointgroupUsecase.Create(ctx, req, userLogin); err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
