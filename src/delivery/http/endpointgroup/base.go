package endpointgroup

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/endpointgroup"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "Endpoint Group HTTP Delivery"

type Handler struct {
	endpointgroupUsecase endpointgroup.Usecase
	resource             *model.Resource
}

func New(endpointgroupUsecase endpointgroup.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		endpointgroupUsecase: endpointgroupUsecase,
		resource:             resource,
	}
}
