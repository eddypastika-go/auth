package endpointgroup

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/common/typeconv"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) DeleteByID(c echo.Context) error {
	var ctx = isession.NewFromEcho(c)

	userLogin, err := ctx.GetUserLogin()
	if err != nil {
		return ctx.JSONError()
	}

	id := typeconv.ConvertStringToUint64(c.Param("id"))
	if err = h.endpointgroupUsecase.DeleteByID(ctx, id, userLogin); err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
