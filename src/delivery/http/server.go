package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/auth/src"
	"gitlab.com/eddypastika-go/utilities/iserver"
)

// Start starting http
func Start(container *src.Container) {
	server := echo.New()

	SetupMiddleware(server, container)
	SetupRoutes(server, NewHandler(container), container.Resource)

	// start app and gracefully shutdown
	iserver.SetupEchoServer(server, container.Config.ToIserverOpt())
}
