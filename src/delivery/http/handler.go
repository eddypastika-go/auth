package http

import (
	"gitlab.com/eddypastika-go/auth/src"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/endpoint"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/endpointgroup"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/health"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/service"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/user"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/useraccess"
	"gitlab.com/eddypastika-go/auth/src/delivery/http/verification"
)

type (
	// Handler is for collecting all Handlers per module.
	// example: user and endpoint
	Handler struct {
		health        *health.Handler
		endpoint      *endpoint.Handler
		endpointGroup *endpointgroup.Handler
		service       *service.Handler
		user          *user.Handler
		userAccess    *useraccess.Handler
		verification  *verification.Handler
	}
)

// NewHandler ...
func NewHandler(container *src.Container) *Handler {
	return &Handler{
		health:        health.New(container.Config),
		endpoint:      endpoint.New(container.EndpointUsecase, container.Resource),
		endpointGroup: endpointgroup.New(container.EndpointGroupUsecase, container.Resource),
		service:       service.New(container.ServiceUsecase, container.Resource),
		user:          user.New(container.UserUsecase, container.Resource),
		userAccess:    useraccess.New(container.UserAccessUsecase, container.Resource),
		verification:  verification.New(container.VerificationUsecase, container.Resource),
	}
}
