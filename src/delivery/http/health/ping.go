package health

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (h *Handler) Ping(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "pong")
}
