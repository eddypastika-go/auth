package health

import (
	"net/http"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"

	"github.com/labstack/echo/v4"
)

func (h *Handler) Hello(c echo.Context) error {
	return c.JSON(http.StatusOK, constant.StartApp)
}
