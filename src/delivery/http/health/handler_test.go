package health

//
//import (
//	"bytes"
//	"encoding/json"
//	"errors"
//	"net/http"
//	"net/http/httptest"
//	"testing"
//
//	"github.com/labstack/echo/v4"
//	"github.com/stretchr/testify/assert"
//	"github.com/stretchr/testify/mock"
//	_mockHealthUseCase "gitlab.linkaja.com/be/gondor/mocks/domain/domain/health/usecase"
//	"gitlab.linkaja.com/be/gondor/model"
//	"gitlab.linkaja.com/be/gondor/model/context"
//	"gitlab.linkaja.com/be/gondor/pkg/config"
//	"gitlab.linkaja.com/be/gondor/pkg/constant"
//	"gitlab.linkaja.com/be/gondor/pkg/log"
//)
//
//func createCtx(reqURL string, cfg *config.Config, ucRes string) (echo.Context, *Handler, *httptest.ResponseRecorder) {
//	e := echo.New()
//	req := httptest.NewRequest(http.MethodGet, reqURL, nil)
//	rec := httptest.NewRecorder()
//	ctx := e.NewContext(req, rec)
//
//	logs := log.New(cfg)
//	ctxVal := context.DefaultValue{
//		Logger:     logs,
//		ThreadID:   "reqId",
//		AppName:    "test gondor",
//		AppVersion: "1.0",
//		SrcIP:      ctx.RealIP(),
//		URL:        ctx.Request().URL.String(),
//		Method:     ctx.Request().Method,
//	}
//	ctx.Set(constant.ContextValue, &ctxVal)
//	ctx.Request().Header.Set("Content-UserType", constant.JSONContentType)
//
//	mockHealthUseCase := _mockHealthUseCase.Usecase{}
//	h := New(&mockHealthUseCase, cfg)
//
//	mockHealthUseCase.On("Readiness", mock.Anything).Return(ucRes)
//
//	return ctx, h, rec
//}
//
//func TestHealthHTTPHandler_Health(t *testing.T) {
//	cfg := config.New()
//	cfg.Logger.Stdout = true
//
//	readyCtx, ready, readyRec := createCtx("/ready", cfg, "res")
//	readyErr := ready.Readiness(readyCtx)
//
//	pingCtx, ping, pingRec := createCtx("/ping", cfg, "API: OK")
//	pingErr := ping.Ping(pingCtx)
//
//	helloCtx, hello, helloRec := createCtx("/", cfg, "gondor is running...")
//	helloErr := hello.Hello(helloCtx)
//
//	healthCtx, health, healthRec := createCtx("/", cfg, "res")
//	healthErr := health.Health(healthCtx)
//
//	tests := []struct {
//		name        string
//		err         error
//		ucRes       string
//		isHealthChk bool
//		rec         *httptest.ResponseRecorder
//	}{
//		{
//			name:  "success for readiness",
//			ucRes: "res",
//			err:   readyErr,
//			rec:   readyRec,
//		},
//		{
//			name:  "success for ping",
//			ucRes: "API: OK",
//			err:   pingErr,
//			rec:   pingRec,
//		},
//		{
//			name:  "success for hello",
//			ucRes: "gondor is running...",
//			err:   helloErr,
//			rec:   helloRec,
//		},
//		{
//			name:        "success for health",
//			isHealthChk: true,
//			err:         healthErr,
//			rec:         healthRec,
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			var _t0 *echo.HTTPError
//			ok := errors.Is(tt.err, _t0)
//			if ok {
//				t.Errorf("should not return HTTPError")
//				return
//			}
//
//			buf := new(bytes.Buffer)
//			_, _ = buf.ReadFrom(tt.rec.Body)
//
//			if tt.isHealthChk == true {
//				var actBody model.Response
//				_ = json.Unmarshal(buf.Bytes(), &actBody)
//				assert.Equal(t, "00", actBody.Status)
//				assert.Equal(t, "Success", actBody.Message)
//				assert.Nil(t, tt.err)
//				return
//			}
//
//			var actualBody string
//			_ = json.Unmarshal(buf.Bytes(), &actualBody)
//			assert.Equal(t, tt.ucRes, actualBody)
//			assert.Nil(t, tt.err)
//		})
//	}
//}
