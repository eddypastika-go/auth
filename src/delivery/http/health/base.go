package health

import "gitlab.com/eddypastika-go/auth/src/shared/config"

type Handler struct {
	config *config.Config
}

func New(config *config.Config) *Handler {
	return &Handler{config: config}
}
