package health

import (
	"net/http"
	"os"
	"runtime"
	"time"

	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/health"

	"github.com/labstack/echo/v4"
)

func (h *Handler) Health(c echo.Context) (err error) {
	var (
		response health.DataResponse
		m        runtime.MemStats
	)

	response.TS = time.Now()
	response.Pid = os.Getpid()
	response.Uptime = time.Since(response.TS)
	response.Status = "ok"
	runtime.ReadMemStats(&m)
	response.Memory.Alloc = m.Alloc
	response.Memory.TotalAlloc = m.TotalAlloc
	response.Memory.Sys = m.Sys
	response.Memory.NumGC = m.NumGC
	response.Memory.HeapAlloc = m.HeapAlloc
	response.Memory.HeapSys = m.HeapSys

	return c.JSON(http.StatusOK, response.ToResponse())
}
