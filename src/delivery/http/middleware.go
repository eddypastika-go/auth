package http

import (
	"net/http"

	"gitlab.com/eddypastika-go/auth/src"

	"gitlab.com/eddypastika-go/auth/src/shared/constant"
	"gitlab.com/eddypastika-go/utilities/mware"

	"github.com/go-playground/validator/v10"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	validate "gitlab.com/eddypastika-go/auth/src/shared/validator"
)

// SetupMiddleware setup echo middleware
func SetupMiddleware(server *echo.Echo, container *src.Container) {
	cfg := container.Config
	server.Use(mware.EchoSessionMiddleware(
		constant.AppCode,
		cfg.AppName,
		cfg.AppVersion,
		cfg.DbDialect,
		container.Logger,
	))
	//server.HTTPErrorHandler = mware.EchoErrorHandler
	server.Use(middleware.BodyDumpWithConfig(mware.LogisMiddleware()))
	server.Use(middleware.Recover())
	server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodOptions},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderAuthorization,
			echo.HeaderAccessControlAllowOrigin,
			"token",
			"Pv",
			echo.HeaderContentType,
			echo.HeaderAccept,
			echo.HeaderContentLength,
			echo.HeaderAcceptEncoding,
			echo.HeaderXCSRFToken,
			echo.HeaderXRequestID,
		},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	server.HideBanner = true
	server.Validator = &validate.CustomValidator{Validator: validator.New()}
}
