package useraccess

import (
	"gitlab.com/eddypastika-go/auth/src/shared/model/dto/useraccess"

	"github.com/labstack/echo/v4"
	"gitlab.com/eddypastika-go/utilities/isession"
)

func (h *Handler) Create(c echo.Context) error {
	var (
		req useraccess.CreateReq
		ctx = isession.NewFromEcho(c)
	)

	if err := ctx.ProcessRequest(msgLog, &req); err != nil {
		return ctx.JSONError()
	}

	userLogin, err := ctx.GetUserLogin()
	if err != nil {
		return ctx.JSONError()
	}

	err = h.uaUsecase.Create(ctx, req, userLogin)
	if err != nil {
		return ctx.JSONError()
	}

	return ctx.JSONNoContent()
}
