package useraccess

import (
	"gitlab.com/eddypastika-go/auth/src/domain/usecase/useraccess"
	"gitlab.com/eddypastika-go/auth/src/shared/model"
)

var msgLog = "User Access HTTP Delivery"

type Handler struct {
	uaUsecase useraccess.Usecase
	resource  *model.Resource
}

func New(uaUsecase useraccess.Usecase, resource *model.Resource) *Handler {
	return &Handler{
		uaUsecase: uaUsecase,
		resource:  resource,
	}
}
