# auth

Authentication and Authorization Service

How to run in Local:

- Run docker:

```
make docker_up
```

- Copy-paste .env.example into .env
- Change value of **TOKEN_SECRET** (ask Eddy)
- Export all config from env file:

```
set -o allexport; source ./resources/.env; set +o allexport
```

- Run app:

```
go run main.go
```

How to run in Local with Live-Reloading:

- Update env for db_host and redis_address with a prefix:

```
docker.for.mac.localhost   
or   
docker.for.win.localhost 
```

- Run without log (Run in Background):

```
make run
```

- See the logs:

```
make logs
```

- Run with log:

```
make run_with_log
```

- Stop app:

```
make stop
```