module gitlab.com/eddypastika-go/auth

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.2.0
	github.com/lib/pq v1.9.0
	github.com/spf13/cast v1.3.1
	gitlab.com/eddypastika-go/utilities v1.0.0
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.3
)
