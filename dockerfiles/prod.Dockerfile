FROM golang:1.16-alpine3.13 as builder
LABEL maintainer="pastikaeddy@gmail.com"

# Add the keys
ARG gitlab_id
ARG gitlab_token

RUN apk update && apk upgrade

# install xz and git
RUN apk add xz && apk add git \
&& rm -rf /var/lib/apt/lists/*

# install upx
ADD https://github.com/upx/upx/releases/download/v3.96/upx-3.96-amd64_linux.tar.xz /usr/local
RUN xz -d -c /usr/local/upx-3.96-amd64_linux.tar.xz | \
    tar -xOf - upx-3.96-amd64_linux/upx > /bin/upx && \
    chmod a+x /bin/upx

# handle private repo
RUN echo "machine gitlab.com login $gitlab_id password  $gitlab_token" > ~/.netrc
RUN export GOPRIVATE="gitlab.com/eddypastika-go"

# define workdir
WORKDIR /go/src/gitlab.com/eddypastika-go/auth

# install modules
COPY go.mod go.sum ./
RUN GO111MODULE=on go mod download

# go build
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o auth .

# compress build file
RUN upx auth

FROM alpine:latest

# add certificates to establish Secured Communication (SSL/TLS)
RUN apk --no-cache add ca-certificates

# define workdir in new stage
WORKDIR /usr/src

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /go/src/gitlab.com/eddypastika-go/auth .

# Command to run the executable
CMD ["./auth"]