FROM golang:1.16-alpine3.13
LABEL maintainer="pastikaeddy@gmail.com"

WORKDIR /go/src/gitlab.com/eddypastika-go/auth

# Add the keys
ARG gitlab_id
ARG gitlab_token

RUN apk update && apk upgrade

# go get gin for auto reload
RUN apk add git
RUN go get github.com/codegangsta/gin

# handle private repo
RUN echo "machine gitlab.com login $gitlab_id password  $gitlab_token" > ~/.netrc
RUN export GOPRIVATE="gitlab.com/eddypastika-go"

# setup dependencies
COPY go.mod go.sum ./
RUN GO111MODULE=on go mod download
COPY . .