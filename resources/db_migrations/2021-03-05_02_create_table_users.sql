create table auth.users
(
    created_at           timestamp with time zone default now(),
    updated_at           timestamp with time zone default now(),
    deleted_at           timestamp with time zone,
    id                   bigserial    not null
        constraint users_pkey
            primary key,
    email                varchar(255) not null,
    password             varchar(255) not null,
    type                 varchar(255) not null,
    source               varchar(20)              default 'webapp'::character varying,
    is_profile_completed boolean,
    is_verified          boolean,
    is_active            boolean,
    constraint empty_check_email_type
        check (((email)::text <> ''::text) AND ((type)::text <> ''::text))
);

create index idx_users_deleted_at
    on auth.users (deleted_at);

create unique index idx_unique_email_type
    on auth.users (email, type);

