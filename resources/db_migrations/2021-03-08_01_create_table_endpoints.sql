create table if not exists auth.endpoints
(
    created_at   timestamp with time zone,
    updated_at   timestamp with time zone,
    deleted_at   timestamp with time zone,
    id           bigserial    not null
        constraint endpoints_pkey
            primary key,
    path         varchar(100) not null
        constraint chk_endpoints_path
            check ((path)::text <> ''::text),
    method       varchar(10)  not null
        constraint chk_endpoints_method
            check ((method)::text <> ''::text),
    type         varchar(20)  not null
        constraint chk_endpoints_type
            check ((type)::text <> ''::text),
    service_name varchar(100) not null
        constraint chk_endpoints_service_name
            check ((service_name)::text <> ''::text),
    updated_by   varchar(100) not null default 'system'::character varying,
    is_active    boolean
);

create unique index if not exists idx_path_method
    on auth.endpoints (path, method);

