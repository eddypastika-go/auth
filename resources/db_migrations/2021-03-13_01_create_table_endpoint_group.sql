create table if not exists auth.endpoint_groups (
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    id bigserial not null constraint endpoint_groups_pkey primary key,
    service_id integer not null,
    endpoint_id integer not null,
    role varchar(100) not null,
    updated_by varchar(100) not null
);
create unique index if not exists idx_endpoint_groups on auth.endpoint_groups (service_id, endpoint_id, role);