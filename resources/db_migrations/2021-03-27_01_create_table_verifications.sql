create table auth.verifications
(
    created_at  timestamp with time zone,
    updated_at  timestamp with time zone,
    deleted_at  timestamp with time zone,
    id          bigserial                                                              not null
        constraint verifications_pkey
            primary key,
    email       varchar(255)                                                           not null,
    user_type   varchar(25)                                                            not null,
    code        varchar(6)                                                             not null,
    template    varchar(100)                                                           not null,
    max_count   bigint                   default 3                                     not null,
    expired_at  timestamp with time zone default (now() + '00:03:00'::interval minute) not null,
    is_verified boolean
);
