create table auth.services
(
    created_at  timestamp with time zone default now(),
    updated_at  timestamp with time zone default now(),
    deleted_at  timestamp with time zone,
    id          bigserial    not null
        constraint services_pkey
            primary key,
    name        varchar(50)  not null,
    code        varchar(50)  not null,
    secret      varchar(100) not null,
    updated_by  varchar(100) not null,
    is_external boolean,
    is_active   boolean,
    constraint empty_check_nae_code_secret_updated_by
        check (((updated_by)::text <> ''::text) AND ((name)::text <> ''::text) AND ((code)::text <> ''::text) AND
               ((secret)::text <> ''::text))
);


create index idx_services_deleted_at
    on auth.services (deleted_at);

create unique index idx_unique_name
    on auth.services (name);

create unique index idx_unique_code
    on auth.services (code);

create unique index idx_unique_secret
    on auth.services (secret);

