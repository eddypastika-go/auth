create table auth.user_accesses
(
    created_at timestamp with time zone default now(),
    updated_at timestamp with time zone default now(),
    deleted_at timestamp with time zone,
    id         bigserial                                                     not null
        constraint user_accesses_pkey
            primary key,
    user_id    integer                                                       not null,
    service_id integer                                                       not null,
    role       varchar(100)             default 'default'::character varying not null,
    updated_by varchar(100)                                                  not null
        constraint empty_check_updated_by
            check ((updated_by)::text <> ''::text),
    is_active  boolean
);


create index idx_user_accesses_deleted_at
    on auth.user_accesses (deleted_at);

