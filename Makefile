SHELL:=/bin/bash

export_env:
	set -o allexport; source ./resources/.env; set +o allexport
docker_up:
	docker-compose up -d --remove-orphans
docker_down:
	docker-compose down
lint:
	golangci-lint run --print-issued-lines=false --exclude-use-default=false --enable=golint --enable=goimports  --enable=unconvert --enable=unparam --enable=gosec --timeout=10m
protoc:
	protoc --go_out=plugins=grpc:. src/shared/model/grpc/*.proto

gin_with_log:
	docker-compose -f docker-compose.dev.yaml up
gin:
	docker-compose -f docker-compose.dev.yaml up -d
run_with_log: docker_up \
	gin_with_log
run: docker_up \
	gin
logs:
	docker container logs -f d-auth
gin_stop:
	docker container stop d-auth
stop: gin_stop \
	docker_down


