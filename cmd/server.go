package cmd

import (
	"context"

	"gitlab.com/eddypastika-go/auth/src"
	"gitlab.com/eddypastika-go/auth/src/delivery/grpc"
	"gitlab.com/eddypastika-go/auth/src/delivery/http"
)

// Start auth
func Start(container *src.Container) {
	// start grpc server
	go func() {
		err := grpc.Start(context.Background(), container)
		if err != nil {
			panic(err)
		}
	}()

	// queue.Start(container)
	http.Start(container)
}
